\chapter{Introducción}

La presencia de procesadores multinúcleo en la mayoría de los dispositivos
condiciona al desarrollador a escalar sus programas mediante la ejecución
paralela. 
Debido a la necesidad de incorporar paralelismo en los programas se han
extendido los
lenguajes de programación, o bien se han desarrollado bibliotecas,
con diversos grados de éxito. El estudio de las formas de incorporar
de manera simple y eficiente la programación paralela al desarrollo
de software sigue siendo objeto de investigación. En particular, en
el lenguaje de programación Haskell~\parencite{Marlow_haskell2010} se han desarrollado una variedad
de extensiones,
bibliotecas, y abstracciones, que permiten
encarar el problema de la programación paralela desde diversos
ángulos.

%Para dar soporte a la experimentación con formas nuevas de
%incorporar paralelismo al desarrollo de software es necesario el
%desarrollo de herramientas adecuadas que permitan el análisis de las
%ejecuciones obtenidas.

El desarrollo de herramientas adecuadas que permitan el análisis de las
ejecuciones obtenidas es necesario al momento de dar soporte a la
experimentación con nuevas formas de incorporar paralelismo.

En este trabajo se desarrolla un lenguaje de dominio específico
embebido en Haskell para permitir la simulación simbólica de programas
paralelos. Se espera permitirle al programador poder observar
directamente qué computaciones se van a paralelizar, permitiéndole tener
un mayor entendimiento sobre la ejecución de su programa. Se presenta
el desarrollo de la herramienta \nombre{}, donde se implementa un lenguaje de
dominio específico destinado a representar programas paralelos,
y se definen representaciones gráficas para la estructura
de programas paralelos.

%Dentro de este capítulo daremos un repaso histórico para dar fundamentos a la
%necesidad de incorporar paralelismo a los programas, y exploraremos las
%consecuencias de hacerlo. Dentro del capítulo~2, se introducirá al lector
%cómo paralelizar dentro de lenguajes funcionales, particularmente el
%paralelismo básico dentro de Haskell. Luego, en el capítulo~3, se mostrará
%el uso de la herramienta \nombre{}, y en el capítulo~4 explicaremos cómo
%fue implementada. Las conclusiones y trabajos futuros se discuten en el
%capítulo~5.

El resto del documento está organizado de la siguiente forma:
\begin{inparaenum}[]{}
    \item en lo que resta de la introducción se presenta el contexto histórico
    para dar fundamentos a la necesidad de incorporar paralelismo a los
    programas, y las consecuencias de hacerlo.
    \item En el capítulo~2, se introduce al lector cómo paralelizar dentro de los
    lenguajes funcionales, particularmente el paralelismo básico dentro de
    Haskell.
    \item En el capítulo~3, se muestra el uso de la herramienta \nombre{},
    \item y en el capítulo~4 explicamos cómo fue implementada.
    \item Las conclusiones y trabajos futuros se discuten en el capítulo~5.
\end{inparaenum}

\section{Historia del CPU multinúcleo}
%Historia de los micros
En 1971 Intel presenta el primer microprocesador de la historia~\parencite{Mazor95thehistory},
el \textbf{Intel 4004}. Éste era un pequeño chip del tamaño de una uña,
%con tan solo 256 bytes de memoria \textit{ROM}, 320 byte de memoria
%\textit{RAM} y un CPU de 4-bit,
el cual vendría a reemplazar las computadoras que en ese momento
eran del tamaño de habitaciones. Este chip se incorporó rápidamente al mercado en
forma de máquinas expendedoras de comida, controladores de ascensores, control de semáforos
e instrumentación médica, entre otros instrumentos electrónicos pequeños.

%Desde su invención hasta la actualidad los microprocesadores
%han sido modificados y desarrollados para satisfacer a un mercado
%que constantemente requiere un aumento del poder de cálculo.

Actualmente los microprocesadores, presentes en la mayoría de los dispositivos electrónicos,
no sólo son estructuras más complejas,
con un poder computacional enorme en comparación a los 4-bits del Intel 4004,
y con capacidad para manejar varios núcleos de procesamiento, sino que
además poseen funciones adicionales, como ser, unidades de punto flotante,
memoria caché, controladores de memoria, y preprocesadores multimedia.
Sin embargo, siguen siendo en esencia iguales al primer microprocesador, un 
chip semiconductor que realiza las computaciones principales del sistema~\parencite{Borkar:2011:FM:1941487.1941507}.

Dentro de dichos avances, el más notable fue el incremento de la capacidad computacional de los
microprocesadores, debido a que al aumentar la velocidad de cómputo el campo de aplicación se aumentaba y
les permitía adquirir nuevas funciones.
El incremento en la velocidad de cómputo de los microprocesadores se debió principalmente
a una técnica denominada \textbf{transistor-speed scaling} presentada por
R. Dennard~\parencite{Dennard74designof}. Dicha técnica permite
reducir la dimensión de los transistores un 30\% en cada
generación\footnote{Aproximadamente 2 años.}
manteniendo el campo de energía constante, dando espacio a aumentar el poder computacional.

Cada vez que se reducía el tamaño de los transistores, el área total se reducía a la mitad,
permitiendo a los fabricantes duplicar la cantidad de transistores en cada generación. Además, para mantener los campos
de energía constantes, al reducir el tamaño también se reducía el consumo de energía de los transistores.
Es decir, en cada generación se obtenían microprocesadores un 40\% más rápidos consumiendo exactamente
la misma cantidad de energía (aunque con el doble de transistores).

Sin embargo, al llegar a tamaños muy pequeños, los transistores no se comportan
como un interruptor perfecto, ocasionando que una porción de la energía utilizada se pierda.
Dicha pérdida de energía volvió prohibitivo continuar aumentando
de esta manera la velocidad de cómputo de los procesadores, por lo que se volvió
necesario buscar otra manera de aumentar el poder computacional.

Como forma alternativa de aumentar la capacidad de procesamiento, se optó por
retomar la idea de aumentar la cantidad de unidades de procesamiento (núcleos)
dentro de un mismo procesador~\parencite{Noguchi:1975:DCH:1499949.1500062}.

\subsection{Problemáticas del Paralelismo}

Cuando se aumenta la frecuencia de los microprocesadores se ejecutan
\textbf{exactamente las mismas} instrucciones más rápido.
Es decir, se ejecutan con mayor velocidad \textbf{los mismos algoritmos} sin modificación
alguna. Dejando al programador simplemente la tarea de
conseguir hardware más potente para que los programas funcionen más rápido,
librándolo de modificar su código o pensar en cómo administrar las computaciones
de su programa.

Al aumentar las unidades de procesamiento se da lugar a la posibilidad de ejecutar
varias instrucciones de manera simultánea, es decir, permite \textit{paralelizar}
la \textbf{ejecución} de los programas. Esto, en teoría, permite multiplicar el poder de cómputo,
aunque como veremos, en la práctica no es tan fácil obtener un mayor rendimiento.
Esto sucede por dos razones:
\begin{itemize}
    \item No todo es paralelizable. Hay algoritmos que son fácilmente paralelizables,
    como por ejemplo, imaginemos que tenemos que pintar una habitación. Si un pintor
    puede pintarla en 16 hs, 4 pintores cada uno con sus herramientas
    pueden dividirse el trabajo y pintarla en 4 hs.
    En cambio, hay tareas que son inherentemente secuenciales, es decir, que no se van
    a poder paralelizar completamente sino que tienen al menos un proceso el cual se tiene que
    realizar de manera secuencial e indivisible. Por ejemplo, supongamos que tenemos que cocinar,
    a un cocinero le llevará 4 hs, y si agregamos otro cocinero más para ayudarlo,
    podrían cocinar mas rápido pero no exactamente el doble de rápido, debido a que hay acciones que se tienen que
    llevar a cabo que no pueden ser divididas. Por ejemplo, no pueden batir la preparación los
    dos juntos.
    \item Paralelizar no es \emph{gratis}. Aún dados algoritmos que son totalmente paralelizables, 
    deberemos saber cómo dividiremos el algoritmo, cuantos núcleos hay disponibles,
    cómo distribuiremos las distintas
    tareas en los diferentes núcleos y cómo se comunican entre ellos 
    en el caso que sea necesario. Por ejemplo, en el caso de los pintores y la habitación,
    deberemos tener la disponibilidad de los 4 pintores, pensar en cómo dividir el trabajo, contar
    con 4 sets de herramientas, etc.
\end{itemize}

Como resultado, el programador debe modificar su código para incorporar la
posibilidad de ejecutar su programa utilizando todos los núcleos disponibles.

\section{Modelos de Paralelismo}
Hoy en día prácticamente todos los dispositivos poseen procesadores multinúcleo,
por lo que la programación paralela se ha vuelto una necesidad.
Para explotar todo el hardware subyacente el programador debe
\textit{pensar} de manera paralela, obteniendo beneficios en velocidad,
con la desventaja que el diseño del software se torna más complicado.
Se debe tener en cuenta dos aspectos: 
\begin{inparaenum}
    \item intrínseco al diseño del algoritmo, identificar qué tareas
    se pueden realizar en paralelo, analizar la dependencia de datos, etc;
    \item hardware disponible, cantidad de núcleos disponibles,
    cómo distribuir las diferentes tareas y establecer canales
    de comunicación entre ellos.
\end{inparaenum}

La programación paralela introduce nuevas problemáticas, como por ejemplo, dado
que en la mayoría de
los lenguajes de programación el programador no tiene acceso a las
políticas de \textit{scheduling},
es decir, no puede asegurar un orden de ejecución sobre diferentes
\textit{hilos} creados, esto produce que el programador
deba garantizar la correcta ejecución sin importar el orden en
que se ejecuten~\parencite{PeytonJones:Parallel:FP}.

Para facilitar la tarea de organizar las computaciones de los programas,
se han planteado diferentes modelos de paralelismo, entre los cuales el más destacado
es el modelo de Flynn.

Michael J. Flynn~\parencite{Flynn:1447203} propone utilizar una clasificación de sistemas
computacionales en base a dos factores, la cantidad de instrucciones que ejecuta 
el microprocesador, y cómo es accedida la información:
\begin{itemize}
    \item 
        \textbf{Single Instruction Stream, Single Data Stream}\textit{(SISD)}: 
        procesadores compuestos por un solo núcleo, el cual ejecuta una sola instrucción
        sobre un segmento de datos particular. Un ejemplo son los procesadores
        con un solo núcleo.
    \item
        \textbf{Single Instruction Stream, Multiple Data Stream}\textit{(SIMD)}:
        procesadores de varios núcleos donde cada uno de ellos 
        ejecuta una sola instrucción (la misma para todos) aunque
        cada uno accede a un segmento de datos diferente. Las unidades de
        procesamiento gráfico (\textit{GPU}) siguen este modelo.
    \item
        \textbf{Multiple Instruction Stream, Single Data Stream}\textit{(MISD)}:
        procesadores de varios núcleos que permiten realizar distintas tareas
        sobre un solo segmento de datos en particular. No se encuentran ejemplos concretos de 
        este modelo de procesadores.
    \item 
        \textbf{Multiple Instruction Stream, Multiple Data Stream}\textit{(MIMD)}:
        procesadores de varios núcleos donde cada uno de ellos es totalmente
        independiente del otro, pudiendo ejecutar diferentes instrucciones cada uno
        y acceder a diferentes segmentos de datos. Dentro de esta categoría
        se encuentran los microprocesadores multinúcleos actuales.
\end{itemize}

Si bien el modelo de Flynn es el más establecido en la literatura, se han propuesto y presentados
otros con mayor nivel de detalle, como ser el trabajo de Skillicorn y Talia~\parencite{Skillicorn:1998:MLP:280277.280278}.

En los centros de cómputo de alto rendimiento el
paralelismo se utiliza a través de computaciones distribuidas,
el cual es modelado mediante sistemas de intercambio de mensajes.
Un ejemplo concreto es \emph{Message Passing Interface}
(MPI)~\parencite{mpi-3.0}. Se basa en proveer un mecanismo de comunicación
entre dos procesos para que estos puedan comunicarse libremente. Dicho mecanismo
se puede implementar de diferentes maneras, aunque en general se utilizan
primitivas de \emph{concurrencia}.

\section{Paralelismo en Lenguajes Funcionales Puros} 
Los lenguajes funcionales puros nos permiten representar las computaciones en términos
de funciones puras, es decir, nos permiten presentar a un programa como una función
que obtiene toda la información necesaria a través de sus argumentos y devuelve algún
valor como resultado de su computación, sin utilizar información externa
a ella que no este explícitamente dentro de sus argumentos, ni modificar nada
fuera del resultado que otorga al finalizar. Por lo tanto todo comportamiento
se vuelve explícito. Esto permite que sea más fácil razonar sobre los programas
y estudiar su comportamiento.

%Determinism
Particularmente al tener computaciones puras la ejecución se vuelve determinista,
el resultado \textbf{sólo} depende de sus argumentos. Por lo tanto, todo programa que se ejecute
secuencialmente tendrá el mismo resultado que su versión paralela con los mismos datos
de entrada, resaltando que el paralelismo es una herramienta para arribar a la solución más rápido.
Esto nos permite razonar sobre los programas paralelos de la misma manera
que los secuenciales y depurarlos sin la necesidad de efectivamente ejecutarlos en paralelo.
Permite al programador independizarse de las políticas de \emph{scheduling}, ya que el orden
de las diferentes tareas que componen al programa puede variar, influyendo posiblemente en el
tiempo de ejecución, pero no en el valor resultado de la misma. No existe la posibilidad
de que haya \textit{deadlock}, excepto en condiciones donde su versión secuencial
no termine debido a dependencias cíclicas~\parencite{Roe91parallelprogramming}.

%Good for parallel V2.
Los lenguajes funcionales, nos permiten delegar todos los aspectos de coordinación al 
\textit{runtime}, es decir, libran al programador de tareas como:
distribuir las diferentes tareas entre los núcleos, establecer canales de comunicación
entre los procesos, la generación
de nuevas tareas e incluso es independiente tanto de la arquitectura
como de las políticas de \textit{scheduling}.
Esto se debe a que los lenguajes funcionales utilizan como
mecanismo de ejecución abstracto la \textit{Reducción de Grafos}
\parencite{Johnsson:1984:ECL:502949.502880,Augustsson:1984:CLM:800055.802038,Kieburtz:1985:GFG:5280.5305}
el cual es fácil de modificar para permitir la evaluación paralela de las computaciones
\parencite{Augustsson:1989:PGR:99370.99386}. Estos sistemas representan a las computaciones
como grafos donde cada hoja es un valor y los nodos, que no son hojas, representan aplicaciones.
Se basan en ir reduciendo cada nodo a su valor correspondiente, posiblemente generando
que otros subgrafos sean evaluados en el proceso.

En particular, Haskell es un lenguaje funcional puro con evaluación \textit{perezosa}.
Las computaciones son evaluadas en el momento que su valor sea necesario o no
evaluadas si no lo son. Al momento de incorporar paralelismo se requiere
otorgar la posibilidad de evaluar expresiones sobre los distintos núcleos disponibles,
posiblemente sin que esas expresiones se necesiten en ese momento, por lo que será
necesario prestar atención cuando se trata de paralelizar una computación.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Se deberá forzar explícitamente la evaluación que se
requiera para una expresión dada, ya que sino se suspenderá su evaluación
y sólo se evaluará (de manera secuencial) cuando se necesite su valor.

\section{Herramientas para el Análisis de Programas Paralelos}
Encontrar problemas en el rendimiento de los programas es una tarea muy complicada,
que requiere de una gran experiencia previa y conocimiento de cómo funciona el
compilador y el entorno de ejecución. Para aliviar esta tarea,
se utilizan herramientas para realizar análisis sobre computaciones paralelas. 

Las herramientas de \emph{profiling} son muy utilizadas debido a que el
comportamiento de los programas paralelos depende del estado general del
sistema.
%Debido a que el comportamiento dentro de la ejecución de programas paralelos
%depende del estado general del sistema al momento de ejecutarlos, son muy
%utilizadas las herramientas de \emph{profiling}.
Estas, con ayuda del \emph{runtime}, toman muestras del comportamiento del
programa durante su ejecución, para luego analizar los resultados e investigar
el origen de problemas de rendimiento, como ser, trabajo de los procesadores
desbalanceado o cuellos de botella.
%Las herramientas de
%\emph{profiling}, con un poco de ayuda del
%\emph{runtime}, van agendando el comportamiento del programa mientras 
%éste es ejecutado para luego analizar los resultados e investigar 

Actualmente Haskell cuenta con \textit{ThreadScope}, es una interfaz gráfica
para el sistema de \textit{profiling} de GHC~\parencite{export:80976,export:79856,Harris:2005:HSM:1088348.1088354},
que permite
dar un análisis de cómo fue el comportamiento del programa, basada en los
eventos que fueron detectados en el ejecución~\parencite{RTS_anatool}.
Si bien se muestra el estado del sistema, momento a momento, no hay
una relación directa (observable) entre la construcción del programa
paralelo diseñado por el programador
y la evaluación de las computaciones. Es decir, el programador
puede observar el comportamiento de todo su programa, pero también parte de su
tarea es determinar, a medida que transcurre la ejecución, a qué parte concreta de su programa
se debe el comportamiento observado.

La extensión de Haskell, Eden~\parencite{Eden}, presenta una herramienta
previa a la creación de \textit{ThreadScope}, llamada
EvenTV~\parencite{Berthold_visualizingparallel},
que de forma muy similar a \textit{ThreadScope} muestra gráficamente, momento a
momento, cómo fueron utilizados los diferentes procesadores. La herramienta
EdenTV utiliza información provista por el monitor de rendimiento \emph{Eden Tracing}.

Dentro del paradigma de programación imperativa se encuentran una gran
variedad de herramientas de análisis de programas paralelos, donde en su mayoría
son herramientas que permiten monitorear el comportamiento del sistema al momento de ejecución,
como ser, el manejo de memoria, el uso de los diferentes núcleos,
o son herramientas de \emph{profiling}, como ser,
ParaGraph, XPVM, Vampir, Scalasca y TUA. Por ejemplo, Vampir es una
herramienta que permite monitorear el comportamiento de la
ejecución de programas paralelos. Puede ser utilizado en diferentes lenguajes
como ser, C o Fortan, y con diferentes librerías como MPI, OpenMP o CUDA.
%
Para mayor información sobre las diferentes herramientas de análisis de
programas paralelos, \citeauthor{CriticalAnalysis_Report} muestran una comparación entre las
herramientas antes mencionadas~\parencite{CriticalAnalysis_Report}.

A su vez, modelos específicos de programación paralela permiten
realizar análisis inherentes al modelo, por lo que se encuentran aun
más herramientas de monitoreo y \emph{profiling}, como ser, sobre el modelo \emph{MPI} se pueden utilizar,
\emph{DEEP/MPI}, \emph{MPE}, \emph{Pablo Performance Analysis Tools}, \emph{Paradyn}, etc.
Estas herramientas se basan en detectar información sobre cuándo se utilizan las llamadas
a las librerías de \emph{MPI}, qué \emph{mensajes} se intercambian mediantes los
canales de comunicaciones establecidos, etc. Para mayor información sobre
comparaciones entre dichas herramientas ver~\cite{Moore:2001:RPA:648138.746655}.

Todas las herramientas mencionadas anteriormente realizan monitoreo de la
actividad del sistema, o bien, realizan \emph{profiling} sobre la ejecución
del programa. En este trabajo se propone analizar la estructura de computaciones
paralelas. Dicha estructura se construye de forma dinámica, pero ésta no depende
del estado del sistema, sino que depende del código escrito.
Ninguna de las herramientas antes mencionadas permiten realizar éste análisis.

\section{Contribuciones}

En este trabajo se desarrolla un lenguaje de dominio específico
embebido en Haskell que permite la simulación simbólica de programas
paralelos. Esta permite al programador observar
directamente qué computaciones se van a paralelizar, dándole
un mayor entendimiento sobre la ejecución de su programa. Además, se presenta
el desarrollo de la herramienta \nombre{}, donde se implementa el lenguaje de
dominio específico destinado a representar programas paralelos. Éste lenguaje
provee un conjunto de operadores que permiten construir programas paralelos
simplemente utilizando operadores similares a los
combinadores de paralelismo puro dentro de Haskell, 
pero a la vez otorgando la posibilidad de realizar gráficos
que representan la evaluación de las computaciones.

Actualmente la herramienta se encuentra alojada en un repositorio \emph{Git}
público, \url{https://bitbucket.org/martinceresa/klytius/}. Se presenta como un
paquete a instalar por la herramienta \emph{Cabal} de la plataforma de Haskell.
