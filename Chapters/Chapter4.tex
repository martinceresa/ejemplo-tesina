\chapter{Implementación}

Este capítulo presenta la implementación de \nombre{} y las técnicas utilizadas.
Primero introducimos los fundamentos de los lenguajes de dominio específico,
luego
plantearemos el problema de observar computaciones compartidas, y por último
explicaremos cómo se generan los gráficos de la estructura dinámica
de los programas vistos en los capítulos anteriores.

\section{Lenguajes de Dominios Específicos}

Una solución prometedora para independizar un sistema de la forma en
que se va a ejecutar el código consiste en la creación de lenguajes
diseñados exclusivamente para facilitar la implementación de la lógica
de negocios en un dominio en particular. Estos lenguajes son conocidos
como lenguajes de dominio específico (DSL). Un DSL permite trabajar a
un nivel de abstracción ideal, donde los conceptos con los que se
trabaja son los propios del dominio de
aplicación~\parencite{Bentley:1986:PPL:6424.315691}.
%
Por ejemplo, un ingeniero puede escribir un programa en MATLAB u otras
herramientas, utilizando primitivas del lenguaje específicas para el
desarrollo de puentes sin preocuparse por el manejo de estructuras
internas de un lenguaje de programación. Otros ejemplos muy
difundidos de DSLs son \LaTeX{} para la edición de texto científico,
y SQL para el manejo de base de datos.

Los DSL tienen la desventaja de que necesitan del desarrollo de
herramientas especiales.
Mientras que para los lenguajes de propósito general están disponibles,
editores, compiladores (lo que incluye parsers, optimizadores y
generadores de código), bibliotecas, generadores de documentación,
depuradores, etc., los DSL no disponen de herramienta alguna.
Es tarea del desarrollador proveer las herramientas necesarias para
su DSL.
Por esto, han adquirido
popularidad los DSLs embebidos (EDSL) en un lenguaje de propósito
general~\parencite{Hudak:1996:BDE:242224.242477}. Un EDSL es un lenguaje definido dentro de otro
lenguaje, lo que permite utilizar toda la maquinaria ya implementada para el lenguaje anfitrión,
disminuyendo enormemente la tarea al momento de implementar el lenguaje.

Al construir un EDSL, diseñamos un lenguaje con ciertas
primitivas, un cierto \textit{idioma} que nos permite manipular elementos y poder
construir un resultado final en un dominio específico.
Dentro de esta construcción podemos optar por dos
caminos~\parencite{Gill:2014:DLC:2611429.2617811}:
\begin{itemize}
    \item \textbf{Embebido Superficial},
    a medida que el lenguaje va identificando instrucciones, manipula
    los valores sobre los cuales se aplica, retornando un nuevo valor. Es decir,
    no se genera estructura intermedia alguna, sino que simplemente los operadores son funciones
    que van operando sobre los valores directamente. El resultado de los
    lenguajes con embebido superficial es un valor, el resultado de la ejecución de las instrucciones
    dadas.
    \item \textbf{Embebido Profundo},
    utilizando las instrucciones se genera un nuevo tipo abstracto de datos,
    permitiendo al usuario construir un \textit{árbol abstracto} de la computación.
    De esta manera, se puede recorrer la estructura, dando la posibilidad de optimizar
    o sintetizar la computación, y luego ser consumida por una función que evalúa y
    otorga la semántica deseada a los constructores. Se logra separar el idioma del
    EDSL de las operaciones mecánicas que se necesitan para evaluar correctamente el resultado,
    permitiendo, por ejemplo, exportar las computaciones en el caso que se quieran ejecutar
    fuera del entorno de Haskell (como ser, en la GPU) o dar varios
    comportamientos diferentes
    a las operaciones del lenguaje. Permite exponer toda la estructura y composición del programa.
\end{itemize}

Para ejemplificar los dos enfoques, supongamos que nuestro idioma está compuesto por
operaciones aritméticas sobre enteros,
más precisamente,
nuestro lenguaje está compuesto por la suma, la resta y la multiplicación.
En el caso del embebido superficial, bastaría con definir un tipo
\haskell{EnteroS} y las siguientes funciones:
\begin{lstlisting}
newtype EnteroS = E Int

suma :: EnteroS -> EnteroS -> EnteroS
suma (E x) (E y) = E (x + y)

resta :: EnteroS -> EnteroS -> EnteroS
resta (E x) (E y) = E (x - y)

mult :: EnteroS -> EnteroS -> EnteroS
mult (E x) (E y) = E (x * y)

runS :: EnteroS -> Int
runS (E x) = x
\end{lstlisting}
Donde, por ejemplo, \haskell{suma (E 4) (E 5) = E 9}.

En el caso de embebido profundo, en cambio, deberíamos definir un nuevo tipo abstracto de datos:

\begin{code}
data EnteroD where
    Lit   :: Int -> EnteroD
    Suma  :: EnteroD -> EnteroD -> EnteroD
    Resta :: EnteroD -> EnteroD -> EnteroD
    Mult  :: EnteroD -> EnteroD -> EnteroD

sumaD :: EnteroD -> EnteroD -> EnteroD
sumaD = Suma
restaD :: EnteroD -> EnteroD -> EnteroD
restaD = Resta
multD :: EnteroD -> EnteroD -> EnteroD
multD = Mult
\end{code}

En el ejemplo antes mencionado, \haskell{sumaD (Lit 4) (Lit 5) = Suma (Lit 4) (Lit 5)}, permitiendo observar directamente la construcción
de la computación.
En el caso que se quiera obtener el resultado de la evaluación, es necesario consumir el árbol abstracto, como se muestra en la
siguiente función:
\begin{lstlisting}
runD :: EnteroD -> Int
runD (Lit n)     = n
runD (Suma x y)  = let
    x' = runD x
    y' = runD y
    in (x' + y')
runD (Resta x y) = let
    x' = runD x
    y' = runD y
    in (x' - y') 
runD (Mult x y)  = let
    x' = runD x
    y' = runD y
    in (x' * y') 
\end{lstlisting}
Utilizando la función \haskell{runD}, se obtiene el resultado de la aplicación de
los operadores que indican los diferentes constructores.
Utilizando dichas definiciones de \haskell{runS} y \haskell{runD}, obtenemos la igualdad
\haskell{runS s = runD d},
para cualquier valor \haskell{s} de tipo \haskell{EnteroS} y \haskell{d} de tipo \haskell{EnteroD}.

Pero en el caso de tener un EDSL profundo, no solo se puede obtener el valor
resultado de evaluar la expresión, sino que, por ejemplo,
se puede contar la cantidad de sumas en una expresión fácilmente sin modificar
las funciones anteriores.
\begin{lstlisting}
contsuma :: EnteroD -> Int
contsuma (Lit _)     = 0
contsuma (Suma l r)  = (contsuma l) + (contsuma r) + 1
contsuma (Resta l r) = (contsuma l) + (contsuma r) 
contsuma (Mult l r)  = (contsuma l) + (contsuma r) 
\end{lstlisting}

De esta manera el embebido profundo combinado con una función específica (como en este caso \haskell{runD}), puede
ser equivalente a un embebido superficial. Además, permite utilizar la construcción de alguna otra manera,
como ser, para extraer más información (\haskell{contsuma}).

En la figura que se muestra a continuación, se ilustra la diferencia entre los EDSL profundos y superficiales.
En el caso de un EDSL superficial, obtenemos un valor, el valor resultante de la aplicación de los diferentes
operadores del lenguaje utilizados por el usuario, mientras que en el caso de un EDSL embebido,
obtenemos un árbol abstracto de las operaciones que fueron aplicadas, de esta manera es posible realizar análisis,
estudiar la estructura obtenida, o ejecutar el programa
\textit{run} y obtener un resultado equivalente
al resultado de un EDSL superficial.

\input{graf.cap4.tex}

Los EDSL profundos nos permiten construir el árbol abstracto de las expresiones del lenguaje que describe.
El árbol abstracto contiene la información de cómo un valor dentro del lenguaje fue construido, por lo que
ya no se manipulan valores, sino árboles abstractos. Ésto trae consigo la imposibilidad de utilizar \textbf{directamente}
estructuras de control de flujo, como \emph{if-then-else}, sin consumir la estructura.
Además al tratar con estructuras complejas, y no con valores manipulables
directamente, se reducen las
optimizaciones que el compilador del lenguaje anfitrión puede realizar.
La construcción de ciertos EDSL puede generar ciclos infinitos. Esto quita la posibilidad de 
recorrer la estructura generada, y genera la necesidad de un mecanismo adicional para la detección de dichos ciclos.

\subsection{EDSL profundo para Paralelismo en Haskell}

En Haskell hay una gran diversidad de abstracciones y DSLs para
modelar paralelismo. Entre los más populares están, la mónada
Eval~\parencite{EvalMonad}, la mónada Par~\parencite{ParMonad},
Data Parallel Haskell~\parencite{DPH}, la librería
Repa~\parencite{REPA}, el lenguaje Accelerate~\parencite{Accelerate},
y el lenguaje Obsidian~\parencite{Obsidian}. Una comparación entre
varios enfoques puede verse en~\parencite{ParallelHakells}.

Dado que nuestro interés está centrado en el estudio del paralelismo esencial dentro de Haskell, desarrollamos
un EDSL profundo en base a los combinadores básicos, \haskell{par} y \haskell{pseq}.
Tomando estos combinadores como constructores del
lenguaje, definimos el tipo de datos como se muestra en el fragmento de Código~\ref{lst:edsl:cap4}

\begin{program}
\begin{lstlisting}
data TPar s where
    Par :: TPar a -> TPar s -> TPar s
    Seq :: TPar a -> TPar s -> TPar s
    Val :: s -> TPar s
\end{lstlisting}
\caption{EDSL profundo para modelar paralelismo.}
\label{lst:edsl:cap4}
\end{program}

Este tipo de datos nos permite modelar el comportamiento dinámico de las computaciones
generadas al momento de ejecutar el programa. Es, además, un tipo de datos con estructura
de árbol, donde las hojas son los valores introducidos por el constructor~\haskell{Val} y
los nodos (que no son hojas) representan 
el combinador \haskell{par}~(\haskell{Par}), y el combinador \haskell{pseq}~(\haskell{Seq}).

Cuando en Haskell utilizamos los operadores básicos \haskell{par} y \haskell{pseq},
el resultado es un comportamiento dinámico que no es observado directamente, sino
que, como ya ha sido mostrado, \haskell{par x y = y} y \haskell{pseq x y = y}. Utilizando
el EDSL planteado en \nombre{} se puede observar la construcción del comportamiento dinámico
de la expresión, ya que se conserva toda la información de los operadores básicos utilizados,
es decir, \haskell{par x' y' = Par x' y'} y
\haskell{pseq x' y' = Seq x' y'}, donde tanto \haskell{x'}, como \haskell{y'}, pueden tener
a su vez comportamiento dinámico adicional.
 
\begin{program}
    \begin{lstlisting}
parsuma :: Int -> Int -> TPar Int
parsuma l r =
    par l' (pseq r' (l' + r'))
    where
       l' = mkVar l
       r' = mkVar r
    \end{lstlisting}
    \caption{}
    \label{lst:parsuma:cap4}
\end{program}

Por ejemplo, para la suma paralela (ver Código~\ref{lst:parsuma:cap4}), obtenemos el siguiente árbol abstracto:
\begin{lstlisting}
parsuma 4 7 = Par (Val 4) (Seq (Val 7) (Val ((+) 4 7) ))
\end{lstlisting}

De esta manera es posible construir un \textit{AST} de las construcciones dinámicas de nuestro programa, que nos permite
\textbf{observar todo el comportamiento dinámico de la computación}. 
El EDSL de \nombre{} provee
primitivas de alto nivel para construir fácilmente dicho \textit{AST}, y luego, mostrarlo
gráficamente, permitiéndole al programador realizar un análisis más exhaustivo del programa.

\section{Observando Computaciones Compartidas}
Una parte vital del desarrollo de la herramienta es la detección de computaciones compartidas.
Éste es un problema muy estudiado
dentro de los EDSL y en optimizaciones de compiladores, dado que las computaciones compartidas
permiten evaluar una única
vez una expresión y luego compartir el resultado, minimizando el trabajo a realizar.
Por ejemplo, en una expresión como \haskell{doble x = let y = x + 1 in y + y},
la expresión \haskell{y} es compartida para la suma (\haskell{y+y}), y su evaluación puede
realizarse una sola vez.

Al desarrollar un programa paralelo en Haskell, el programador, genera
\emph{sparks} para la evaluación
de expresiones que se podrían utilizar luego.
%\footnote{O no, dando origen al denominado Paralelismo Especulativo}.
Es decir, cuando
tenemos una expresión como:
\begin{lstlisting}
    par x m
\end{lstlisting}
la computación \haskell{x} es luego utilizada en \haskell{m}.
Esto mismo, podemos verlo directamente en una expresión como \haskell{par y y}. Nuestro objetivo
es representar las computaciones de manera gráfica, por lo que deberíamos identificar en este caso a
\haskell{y} como una referencia a la misma computación, así como identificar todas las referencias de \haskell{x}
que ocurran dentro de \haskell{m}.
%como la misma computación.

En un EDSL profundo las computaciones son representadas como un árbol abstracto, por lo que las computaciones
recursivas o que poseen referencias mutuas pueden dar lugar a un árbol \textit{infinito}\footnotemark.
\footnotetext{Podemos hablar de infinito gracias a la evaluación lazy.}

Andy Gill presenta un mecanismo de detección de computaciones compartidas, utilizando funciones dentro de la mónada IO de Haskell
y funciones de tipo~\parencite{Chakravarty:2005:ATS:1090189.1086397}. Esta solución nos permite construir
el árbol abstracto utilizando primitivas
de nuestro EDSL y luego generar el grafo que representa nuestra computación, y por lo tanto
nos permite observar las computaciones compartidas~\parencite{Gill:2009:TOS:1596638.1596653}.

En dicha solución introduce la función \haskell{reifyGraph} (de tipo como se muestra a continuación)
que, dado un elemento \haskell{x} de tipo \haskell{t} construye un grafo de tipo \haskell{Graph},
el cual representa al elemento \haskell{x} detectando las computaciones compartidas.
La clase \haskell{MuRef} provee las operaciones necesarias para poder observar la estructura de
un tipo dado, y para construir grafos con las computaciones compartidas. Para definir una instancia
es necesario dar una función de tipo \haskell{DeRef}, que actúa como un sinónimo,
entre el tipo a instanciar y su representación como un grafo, %nodos en el grafo,
y una función \haskell{mapDeRef} que permite recorrer el árbol y transformar sus elementos en el
nodo que los representa dentro del grafo final.
\begin{lstlisting}
    reifyGraph :: (MuRef t) => t -> IO (Graph (DeRef t))
\end{lstlisting}
En Haskell se define de la siguiente manera:
\begin{lstlisting}
class MuRef a where
    type DeRef a :: * -> *
    mapDeRef     :: (Applicative f) =>
                    (a -> f u)
                 -> a
                 -> f (DeRef a u)
\end{lstlisting}
El primer argumento de \haskell{mapDeRef} es una función que es aplicada a los
hijos del segundo argumento, el cual es el nodo que se está analizando.
El resultado es un nodo que contiene valores únicos de tipo \haskell{u}
que fueron generados en base al primer argumento. La función \haskell{mapDeRef} utiliza
funtores aplicativos~\parencite{mcbride08:applicative-programming} para proveer
los efectos necesarios para la correcta asignación de los valores únicos.

A continuación mostramos como definir la representación de un árbol de tipo \haskell{TPar},
mediante el tipo \haskell{Node}.
Notar que en el caso de la transformación
de \haskell{Val'} (\haskell{TPar}) a \haskell{Val}(\haskell{Node}), dado en la definición
de \haskell{mapDeRef}, el valor es ignorado,
ya que no es necesario conservar el elemento,
por lo que se representa y se trata como una caja cerrada.

\begin{lstlisting}
data TPar s where
    Par' :: TPar a -> TPar s -> TPar s
    Seq' :: TPar a -> TPar s -> TPar s
    Val' :: s -> TPar s

data Node s where
    Val :: Node s
    Par :: s -> s -> Node s
    Seq :: s -> s -> Node s
    deriving Show
        
instance MuRef (TPar s) where
    type DeRef (TPar s) = Node
    mapDeRef f (Par' l r)  = Par  <$> f l <*> f r
    mapDeRef f (Seq' l r)  = Seq  <$> f l <*> f r
    mapDeRef f (Val' _)    = pure Val 
\end{lstlisting}

Utilizando la definición de \haskell{MuRef (TPar s)},
%y la libreria desarrollada
%por Andy Gill en Haskell
permite detectar las computaciones
compartidas una vez obtenido un valor de tipo \haskell{TPar s}, permitiendo al usuario
construir su programa normalmente (sin obligar el uso de mónadas), y luego aplicar la función
\haskell{reifyGraph}, ahora sí dentro de la mónada \haskell{IO}.

Por ejemplo en el caso de la suma paralela (Código~\ref{lst:parsuma:cap4}), el resultado de aplicar la función
\haskell{reifyGraph} es:
\begin{lstlisting}
reifyGraph (parsuma 4 7) =
    let 
        [(1,Par 2 3),(3,Seq 4 5),(5,Val ("(+) 4 7")),(4,Val ("7")),(2,Val ("4"))]
    in 1
\end{lstlisting}
El resultado es una lista de pares compuestos por el valor único que representa al nodo, y el constructor
correspondiente, y como resultado el valor único de la raíz del árbol. A modo descriptivo se introducen
etiquetas que no están representadas en el constructor dado anteriormente.

%% Otras Sol
%Otras soluciones al problema de detectar computaciones compartidas y las razones por las que no fueron elegidas son:
Existen otras soluciones al problema de detectar computaciones compartidas. A continuación detallamos
algunas de estas y explicamos por qué no fueron utilizadas en este trabajo.
\begin{itemize}
    \item Extensión a los
    lenguajes funcionales que permite observar (y por ende recorrer) este tipos de estructuras~\parencite{Claessen99observablesharing}.
    La solución consiste en poder definir un tipo de datos \haskell{Ref}, con tres operaciones. Permitir la creación de referencias
    a partir de un elemento, poder obtener el elemento a través de su referencia, y poder comparar referencias.
    Es una solución no conservativa,
    la cual implica tener que modificar el lenguaje anfitrión, en este caso Haskell.
    \item Etiquetado explícito, etiquetar a los nodos del árbol con un elemento único. En este caso, el usuario es el que provee
    las etiquetas manualmente.
    \item Las mónadas pueden ser utilizadas para generar las etiquetas de manera implícita, o bien, utilizar funtores aplicativos.
    El problema de esta solución es que impacta directamente en el tipo de las primitivas básicas de la herramienta, obligando al
    usuario a utilizar un modelo monádico, obscureciendo el código. Es posible utilizar llamadas inseguras a la mónada \haskell{IO},
    y utilizar, lo que en programación imperativa se conoce como un contador, rompiendo las garantías que Haskell provee.
\end{itemize}

La solución elegida en el presente trabajo no exhibe los problemas descriptos.
Permite al usuario
de \nombre{} el desarrollo libre de efectos monádicos generados por la detección
de computaciones compartidas, sin tener que modificar el EDSL presentado en este
trabajo, ni Haskell.

\section{Graficando Computaciones}

Utilizando la función de \haskell{reifyGraph}, logramos generar un grafo que representa la
computación propuesta por el \textit{AST}, detectando las computaciones compartidas
como se explicó en la sección anterior.
Para generar la figura 
utilizamos la librería para la herramienta \textit{Dot} dentro de la suite \textit{GraphViz}~\parencite{DOT_man,GraphViz,Gansner00anopen}. 

Dot es una herramienta muy versátil que permite dibujar grafos dirigidos, y se encarga de distribuir los nodos y las
aristas sobre un mapa cartesiano. La herramienta toma como entrada un archivo de texto
descriptivo del grafo y genera el archivo de salida con la imagen del grafo. Para generar dichos archivos de texto
que representan los grafos a graficar, utilizamos la librería de Haskell, \textit{GraphViz}~\parencite{GraphViz_hask}.

La librería se basa en un tipo de datos \haskell{GraphvizParams n nl el cl l}, que contiene toda la configuración gráfica
del grafo, donde se indica que el grafo tiene nodos de tipo \haskell{n}, las etiquetas de los nodos son de tipo \haskell{nl},
la etiqueta de las aristas de tipo \haskell{el}, y el resto corresponden a tipos para realizar \textit{clustering} de nodos.
\\
\minipage{\textwidth}
\hspace{1.5em}El tipo de datos \haskell{GraphvizParams n nl el cl l} es un \emph{record}
compuesto, entre otra información utilizada para realizar \textit{clustering} de
nodos, por:
\begin{compactitem}
    \item \haskell{isDirected :: Bool}, donde se indica \haskell{True} si el grafo es dirigido y \haskell{False} sino.
    \item \haskell{globalAttributes :: [GlobalAttributes]}, donde se especifican atributos generales a todo el grafo.
    \item \haskell{fmtNode :: (n, l) -> Attributes}, donde se indican los atributos de los nodos.
    \item \haskell{fmtEdge :: (n, n, el) -> Attributes}, donde se indican los atributos de las aristas.
\end{compactitem}
\endminipage
\vspace{0.5em}

La librería pone a disposición constructores de parámetros básicos, en conjunto con funciones para generar el grafo
Dot, exportar el archivo de texto y utilizando la herramienta externa, generar el archivo con formato \emph{Pdf}.

Definiendo un elemento de tipo \haskell{GraphvizParams}, el cual contiene las propiedades gráficas
de la imagen que se obtiene como resultado, se define una función, \haskell{graficar}(ver Código~\ref{lst:graficar}),
que toma como argumento, un valor booleano indicando si se requiere mostrar información adicional,
un elemento de tipo \haskell{TPar s}, y el nombre del archivo a generar. Una vez
obtenidos los argumentos, la función genera un archivo con el gráfico de la
computación en el mismo directorio donde se ejecutó el programa.

\begin{program}
\begin{lstlisting}
graficar :: Bool -> TPar a -> String -> IO FilePath
graficar b g s = do
    g' <- reifyGraph g
    runGraphvizCommand Dot (graphToDot (grafiParams b False 0.25) (graph x)) Pdf (s++".pdf")
\end{lstlisting}
    \caption{Función \haskell{graficar}}
    \label{lst:graficar}
\end{program}

La función \haskell{graficar} permite obtener las representaciones gráficas de los diferentes
programas resultantes de utilizar a \nombre{}, en el Cuadro~\ref{tab:tpar:gra} se muestran las representaciones
de los constructores utilizados.

\begin{table}
    \centering
    \begin{tabular}{| m{0.4\textwidth} | m{0.4\textwidth} |}
        \hline
            \hspace{3.5em} Par Val Val & \includegraphics[scale=0.6]{parvalval.pdf} \\
            \hline 
            \hspace{3.5em} Seq Val Val & \includegraphics[scale=0.6]{seqvalval.pdf} \\
        \hline
    \end{tabular}
    \caption{Constructores de \haskell{TPar} con su representación gráfica asociada.}
    \label{tab:tpar:gra}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\subsection{Etiquetado de Nodos en \nombre{}}

El usuario tiene la habilidad de introducir etiquetas mediante la utilización de
primitivas dentro de \nombre{}, que se muestran a continuación.

\begin{code}
    pars :: String -> TPar a -> TPar b -> TPar b
    seqs :: String -> TPar a -> TPar b -> TPar b
    smap :: String -> (a -> b) -> TPar a -> TPar b
    mkVar' :: String -> TPar a
    mkVars :: (Show a) => TPar a
\end{code}

Utilizando estos constructores se pueden introducir etiquetas
que serán mostradas dentro de los nodos del gráfico resultante.
Las etiquetas no manipulan de manera alguna los valores, son simplemente
atributos gráficos que permiten al programador identificar a qué expresión
corresponde cada nodo del gráfico.
Podemos ver como las etiquetas son mostradas en la Figura~\ref{fig:etiq}

\begin{figure}
    %\centering
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=0.9\textwidth]{etqpar.pdf}
        \caption{Etiqueta dentro del nodo que representa a \haskell{pars "Etiqueta Par" a b}}
        \label{fig:etiqpar}
    \end{subfigure}
    %~
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{etqseq.pdf}
        %\includegraphics[scale=0.5]{etqseq.pdf}
        \caption{Etiqueta dentro del nodo que representa a \haskell{seqs "Etiqueta Seq" a b}}
        \label{fig:etiqseq}
    \end{subfigure}
    \caption{Etiquetas dentro de los nodos}
    \label{fig:etiq}
\end{figure}

Es recomendable el uso de la etiquetas, ya que permiten interpretar mejor los valores representados. Sin
embargo son un arma de doble filo, debido a que se muestra lo que usuario
expone, y no cuál es el valor real a representar.

Por ejemplo, tomando un ejemplo ya visto en los capítulos anteriores, podemos paralelizar la aplicación de 
una función sobre una lista, y vamos a tener una estructura dinámica como resultado. En este caso, las etiquetas
pueden ayudar a ver si la forma normal débil a la cabeza es profundidad
suficiente para la evaluación de las expresiones paralelizadas. Se puede observar
la diferencia en los gráficos obtenidos en la Figura~\ref{fig:etiqshow}

\begin{figure}
\begin{subfigure}[b]{\textwidth}
\includegraphics[width=\textwidth]{listsinetiq.pdf}
\caption{Paralelizar una lista sin etiquetas}
\end{subfigure}
\\
\begin{subfigure}[b]{\textwidth}
\includegraphics[width=\textwidth]{listconetiq.pdf}
\caption{Paralelizar una lista mostrando todas las etiquetas}
\end{subfigure}
    \caption{Diferencia entre mostrar etiquetas o no mostrarlas}
    \label{fig:etiqshow}
\end{figure}

Utilizando etiquetas el usuario puede indicar que valores son expresados dentro de las ``cajas``.
Además el usuario puede agregar etiquetas descriptivas a los diferentes nodos que representan la estructura
dinámica de control de paralelismo.

Por ejemplo, en el caso que quisiéramos sumar todos los enteros de una lista, para explotar
el paralelismo es conveniente realizar un poco más de trabajo, como contar los elementos de la lista
y luego evaluar de forma paralela la sumatoria de la primer mitad, computar la
segunda mitad y luego realizar
la suma de los valores resultantes (ver Código~\ref{lst:suml}). Podemos observar en la Figura~\ref{fig:suml}
el contraste entre los gráficos resultantes
de utilizar etiquetas sobre los nodos de comportamiento dinámico, y no utilizar etiquetas sobre estos.

\begin{program}
\begin{lstlisting}
suml :: Int -> [Int] -> TPar Int
suml _ []  = mkVars 0
suml _ [x] = mkVars x
suml n xs  = par lf (pseq rf (lf+rf))
    where
        nn = div n 2
        (lf,rf) =   (suml nn (take nn xs)
                    ,suml nn (drop nn xs))

sl :: Int -> [Int] -> TPar Int
sl _ []  = mkVars 0
sl _ [x] = mkVars x
sl n xs  = pars "FPart" lf (seqs "SPart" rf (lf+rf))
    where
        nn = div n 2
        (lf,rf) =   (sl nn (take nn xs)
                    ,sl nn (drop nn xs))

ejemplodin :: IO FilePath
ejemplodin = do
    let ej = ([2,3,4,1] :: [Int])
    let ln = length ej
    graficar False (suml ln ej) "suml"
    graficar False (sl ln ej) "sumletiq"
\end{lstlisting}
\caption{Sumatoria de una lista de enteros de forma paralela.}
\label{lst:suml}
\end{program}

\begin{figure}
\begin{subfigure}[b]{\textwidth}
\includegraphics[width=\textwidth]{sumaloca.pdf}
\caption{Paralelizar una lista sin etiquetas}
\end{subfigure}
\\
\begin{subfigure}[b]{\textwidth}
\includegraphics[width=\textwidth]{sumalocaetiq.pdf}
\caption{Paralelizar una lista mostrando todas las etiquetas}
\end{subfigure}
    \caption{Diferencia entre mostrar etiquetas o no mostrarlas}
    \label{fig:suml}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Para obtener las representaciones gráficas resultantes de utilizar a \nombre{}, las cuales fueron mostradas
en todo el trabajo, es necesario realizar algunas definiciones de la información que llevaran los nodos y etiquetas.
Definimos los tipos \haskell{Lbl} y \haskell{NLbl}, para modelar las aristas y los nodos respectivamente, como se muestra 
a continuación.

\begin{code}
data Lbl where -- Aristas
    Spark   :: Lbl
    LSeq    :: Lbl
    PExec   :: Lbl
    Exec    :: Unique -> Lbl

data NLbl where -- Nodos
    NPar :: TLabel -> NLbl
    NSeq :: TLabel -> NLbl
    NVal :: TLabel -> NLbl
\end{code}

Definiendo los tipos de datos de las aristas y nodos, podemos identificar los
diferentes elementos generados del grafo reificado. Esto nos permite asignarles
diferentes propiedades gráficas, como ser,
los diferentes estilos de aristas vistos (punteada, contigua, o compuesta por guiones).
%
Los nodos llevan
información adicional:
una etiqueta que luego se podrá mostrar en el gráfico
para identificar las computaciones.

Mostraremos cómo se da formato a algunos casos, para que sea más fácil la lectura.
Definimos dos funciones, \haskell{nodeSTD} y \haskell{edgeSTD} (Código~\ref{lst:nesty}),
las cuales utilizamos dentro de \nombre{} para dar un formato gráfico a las computaciones, el
cual puede ser modificado fácilmente. 

\begin{program}
    \begin{lstlisting}
nodeSTD :: Bool -> (Unique, NLbl) -> Attributes
nodeSTD b (x,l@(NPar s))  =
    case b of
         True  -> [labelStyle (show x) s, shapeNods l]
         False -> [labelStyle "" s, shapeNods l]
nodeSTD b (x,l@(NSeq s))  =
    case b of
         True  -> [labelStyle (show x) s, shapeNods l]
         False -> [labelStyle "" s, shapeNods l]

edgeSTD :: Bool -> (a,b,Lbl) -> Attributes 
edgeSTD (s,d, Spark) = [Style [SItem Dashed []]] 
edgeSTD (s,d, GEdge) = [Style [SItem Dotted []]] 
edgeSTD (s,d, LSeq)  = [Dir NoDir,Style [SItem Bold []]]
edgeSTD (s,d,Exec x) = [Style [SItem Bold [], SItem Solid []]] 
edgeSTD (s,d,l)      = [emptyLbl]
    \end{lstlisting}
    \caption{Estilo por defecto para nodos de tipo \haskell{NPar} y aristas que denotan un spark.}
    \label{lst:nesty}
\end{program}

Las funciones \haskell{nodeSTD} y \haskell{edgeSTD}, son las que definen el estilo de los nodos y las aristas
utilizadas en la definición de \haskell{grafiParams}, como \haskell{fmtNode} y \haskell{fmtEdge} respectivamente.

Gracias al alto orden de las funciones en Haskell, y que el tipo de datos \haskell{Attributes} es simplemente
una lista de atributos, es muy sencillo redefinir los atributos de los diferentes elementos,
en el caso que el usuario quisiera realizar modificaciones visuales.

\subsection{Manipulación de elementos dentro de \nombre{}}

Para facilitar el uso de la herramienta, se implementan mecanismos de
manipulación de elementos embebidos en el EDSL. Esto se logra
mediante las instancias de funtor,
aplicativo, y mónada del EDSL, como se muestra en el fragmento de
Código~\ref{lst:final}.
%
\begin{program}
\begin{lstlisting}
instance Functor (TPar a) where
    fmap f (Par l r) = Par l (fmap f r)
    fmap f (Seq l r) = Seq l (fmap r)
    fmap f (Val x)   = Val (f x)

instance Applicative TPar where
    pure = Val
    (Par l f) <*> x = Par l (f <*> x)
    (Seq l f) <*> x = Seq l (f <*> x)
    (Val f)   <*> x = fmap f x

instance Monad TPar where
    return = Val
    (Par l r)  >>= f = Par l (r >>= f)
    (Seq l r)  >>= f = Seq l (r >>= f)
    (Val x)    >>= f = f x
\end{lstlisting}
\caption{EDSL para paralelismo y sus instancias.}
\label{lst:final}
\end{program}
%
%Las instancias son necesarias para la manipulación de valores dentro del tipo
%\haskell{TPar}, dado que no disponemos manera de representar la aplicación de funciones
%dentro del EDSL,
%debemos proveer un mecanismo para 
%que el usuario final pueda utilizar el EDSL de manera sencilla.
%
%Como se observa en el Código~\ref{lst:final},
Todas las instancias aplican la transformación
sobre el hijo derecho de los constructores. Esto se debe a la semántica que representan; recordemos
que \haskell{par a b = b} (igual que \haskell{pseq a b = b}), por lo que si aplicamos una función,
\haskell{f (par a b) = f b} (igual que \haskell{f (pseq a b) = f b}).

\begin{program}
\begin{lstlisting}
resum = map (fmap (+1)) [Just 23, Just 4, Just 79014] `using`
    (parList rdeepseq)
const2 :: a -> b -> b
const2 _ y = y
\end{lstlisting}
\caption{Efectos dinámicos lazy.}
\label{lst:lazy:effects}
\end{program}

Utilizando la instancia aplicativa en el caso que se obtenga una función con cierto comportamiento
dinámico asociado, (\haskell{f :: TPar (a -> b)}), y un elemento con cierto comportamiento, (\haskell{x :: TPar a}),
podemos aplicar la función sobre el elemento (\haskell{f <*> x}), acumulando los efectos necesarios para determinar el valor
de \haskell{f}, y luego los efectos necesarios para determinar el valor de \haskell{x}, y por último, realizar la aplicación
de la función, \haskell{f x}. Dicha acumulación de comportamiento dinámico no concuerda con la evaluación perezosa de los
efectos dinámicos. Por ejemplo, dentro del Código~\ref{lst:lazy:effects}, podemos tener un valor con cierto
comportamiento dinámico \haskell{resum}, y una función \haskell{const2} que
toma dos argumentos y devuelve el segundo. Nos podemos preguntar si al evaluar
la expresión \haskell{const2 resum 42}, todo, nada, o alguna parte del comportamiento
dinámico puede ser disparado. Como indica la evaluación perezosa, a menos
que la función sea estricta, el comportamiento dinámico no será evaluado.

Dentro de este capítulo repasamos las técnicas utilizadas para la implementación
de la herramienta, definimos las bases teóricas para la creación de un EDSL
profundo, cómo detectar las computaciones compartidas, cómo son graficadas las
estructuras dinámicas y por último cómo son operados los elementos dentro del
EDSL.
