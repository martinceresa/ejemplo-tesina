\chapter{Paralelismo en Lenguajes Funcionales}

En
este capítulo se realiza una breve introducción a la programación
paralela dentro del paradigma de programación funcional, particularmente dentro
de Haskell. Se exploran las diferentes posibilidades de particionar un programa,
se analizan los combinadores básicos de paralelismo puro dentro de Haskell, y 
el impacto de la evaluación \emph{perezosa} sobre la programación
paralela. Por último, se construye un modelo más abstracto sobre los
combinadores básicos, que permite separar los algoritmos del comportamiento
dinámico.

\section{Particionamiento de programas}

Dada la posibilidad de ejecutar diferentes computaciones sobre diferentes núcleos,
se introduce un nuevo requerimiento dentro del diseño del algoritmo,
que es decidir cuáles son las computaciones que se van a distribuir, o
cómo \emph{particionar} los programas para arribar más rápido la solución.

Dentro de los lenguajes funcionales
existe un gran espectro
de opciones para explotar el paralelismo presente en los programas.
Podemos destacar dos estrategias básicas para decidir cómo particionar un
programa~\parencite{Hammond94parallelfunctional}:
\begin{itemize}
    \item \textbf{particionamiento implícito:}
       El compilador luego de un análisis estático del código es quien decide
       cuáles son las tareas a realizar en paralelo. Este tipo de paralelismo automático
       puede generar tareas muy pequeñas en comparación con
       el costo de realizarlas en paralelo e incluso, por dependencia de datos, podría
       no generar paralelismo alguno.
    \item \textbf{particionamiento explícito:}
       El programador es quien decide cuáles tareas se realizarán en paralelo,
       debiendo tener en cuenta el tamaño de las tareas a paralelizar, el
       tiempo de ejecución de las mismas, la cantidad de núcleos disponibles, etc.
       Si bien el resultado es bueno, el programador
       debe dedicarle más tiempo y esfuerzo al diseño del algoritmo
       mezclando el código con primitivas de paralelismo del lenguaje.
\end{itemize}

Los lenguajes que utilizan particionamiento implícito definen un lenguaje
de bajo nivel para el control de paralelismo, el cual no está disponible para
uso directo por parte del programador, sino que, es el compilador quien
detecta estáticamente cómo particionar un programa e inserta automáticamente el lenguaje
de control de paralelismo. Un ejemplo de lenguaje de control es el uso de
\emph{combinadores seriales}~\parencite{Hudak:1985:SCL:5280.5304}. El enfoque implícito
tiene como ventaja que
el programador no tiene contacto con el paralelismo, por lo que
notará que sus
programas utilizan el hardware disponible en la computadora de manera automática.
El problema con estos métodos automáticos es que el paralelismo obtenido tiende a ser demasiado
\emph{granular}, es decir que las computaciones son relativamente pequeñas,
por lo que el costo de generar el paralelismo (crear el hilo, establecer canales de comunicación,
etc) puede ser mayor que el beneficio obtenido.

\begin{program}
    \begin{lstlisting}
left pid = 2*pid
right pid = 2*pid+1

parsum1 f g x = (f(x) $on 0) + (g(x) $on 1)

parsum2 f g x = (f(x) $on left($self)) + (g(x) $on right($self))
    \end{lstlisting}
    \caption{Ejemplo de anotaciones}
    \label{lst:annotations}
\end{program}

Por otro lado, los lenguajes explícitos utilizan técnicas como
anotar en el programa
%explícitamente
el control del paralelismo~\parencite{Hudak:1986:PP:19644.19650}. Estas
anotaciones forman parte de un \emph{meta-lenguaje}
dedicado a controlar el paralelismo, donde el programador
es el encargado de distribuir las computaciones en los distintos procesadores.
Por ejemplo, utilizando el operador infijo \haskell{$on} para indicar en qué procesador
queremos evaluar la expresión, podemos escribir la suma de dos expresiones
de forma paralela en dos procesadores, como se muestra en el Código~\ref{lst:annotations}.
En la primer definición \haskell{parsum1},
se establece que \haskell{f(x)} se evaluará en el procesador 0
y \haskell{g(x)} en el procesador 1, mientras que en la segunda
definición, \haskell{parsum2}, los identificadores de los procesadores se generan dinámicamente
en base al procesador actual (\haskell{$self}).

Los lenguajes de particionamiento explícito otorgan al programador
mayor control sobre el paralelismo, con la desventaja que
el código resultante es una mezcla (muy acoplada) entre el código del algoritmo
y código de control. Incluso hay que modelar
el hardware subyacente, como se observa en el Código~\ref{lst:annotations}, donde utilizamos
una estructura de árbol binario para acceder a los identificadores de los procesadores.

\section{Paralelismo Semi-Explícito}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{parab.pdf}
        \caption{\haskell{par a b}}
        \label{fig:parab}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{pseqab.pdf}
        %\includegraphics[scale=0.6]{pseqab.pdf}
        \caption{\haskell{pseq a b}}
        \label{fig:pseqab}
    \end{subfigure}
    \caption{Comparación gráfica entre \haskell{par} y \haskell{pseq}}
    \label{fig:parpseq}
\end{figure}

Un enfoque intermedio entre el paralelismo explícito
donde toda la coordinación, comunicación y control es
de manera explícita (diseñado por el programador) y el paralelismo implícito donde
no se tiene control alguno sobre el paralelismo, es el \emph{particionamiento semi-explícito}.
En este enfoque, el programador introduce
ciertas anotaciones en el programa pero todos los
aspectos de coordinación son delegados al entorno de ejecución~\parencite{LoidlTHZB08}.

%Se presentan dos primitivas básicas de coordinación de computaciones con sus tipos
%como se muestra a continuación.
El lenguaje de programación Haskell, cuenta con dos combinadores básicos de coordinación de computaciones.
\begin{lstlisting}
    par,pseq  :: a → b → b
\end{lstlisting}
Ambos combinadores son los únicos con acceso al control del paralelismo del programa\footnote{Si bien existen otros
métodos, en general no se recomienda mezclar varias técnicas.}. 

Denotacionalmente ambas primitivas son proyecciones en su segundo argumento.
\begin{lstlisting}
par  a b = b
pseq a b = b
\end{lstlisting}
%que \haskell{par a b = b}, al igual que \haskell{pseq a b = b}.
Operacionalmente
\lstinline{pseq} establece que el primer argumento \textbf{debe} ser evaluado antes que el segundo,
mientras que 
\lstinline{par} indica que el primer argumento \textbf{puede} llegar a ser evaluado
en paralelo con la evaluación del segundo. Utilizando los combinadores básicos \haskell{par} y \haskell{pseq}
el programador establece el \textbf{qué} y \textbf{cómo} paralelizar sus programas.
Podemos representarlo de manera gráfica
como se muestra en la Figura~\ref{fig:parpseq}: \haskell{par} marca la computación \haskell{a}
para ser evaluada en paralelo (línea punteada), y continúa con la evaluación de
\haskell{b} (línea continua), mientras que,
\haskell{pseq} establece que para evaluar \haskell{b} primero se deberá evaluar \haskell{a}.
Ambos combinadores proponen la evaluación de sus argumentos: \haskell{par a b} propone que \haskell{a}
se evalúe en otro núcleo, de forma independiente de \haskell{b}, mientras que \haskell{pseq a b}
establece que antes de evaluar \haskell{b} se debe evaluar \haskell{a}.

Toda computación, \haskell{a}, marcada por \haskell{par a b}, será vista por el
entorno de ejecución como una oportunidad de
trabajo a realizar en paralelo, la cual se denomina \emph{spark}.
La acción de \emph{sparking} no fuerza inmediatamente la creación de un \emph{hilo},
sino que es el \emph{runtime} el encargado de determinar cuáles \emph{sparks} van a ejecutarse,
tomando esta decisión
en base al estado general del sistema, como ser, si encuentra disponible a algún núcleo en el cual ejecutar la evaluación del \emph{spark}.
El \emph{runtime} es también el encargado de manejar los detalles de la ejecución de computaciones en paralelo,
como ser, la creación de hilos, la comunicación entre ellos, el \emph{workload balancing}, etc.

\begin{table}
    \begin{tabular}{l r}
        \begin{subfigure}[b]{0.4\textwidth}
\begin{lstlisting}
parsuma :: Int → Int → Int
parsuma x y = par x (x+y)
\end{lstlisting}
            \caption{Suma paralela}
            \label{lst:parsuma}
        \end{subfigure}
        & 
        \begin{subfigure}[b]{0.5\textwidth}
            \includegraphics[width=\textwidth]{sumapar.pdf}
            %\includegraphics[scale=0.7]{sumapar.pdf}
            \caption{Representación gráfica}
            \label{fig:sumapar}
        \end{subfigure} \\
    \end{tabular}
    \caption{Suma utilizando par}
    \label{tab:sumapar}
\end{table}

Intentar paralelizar la evaluación de una expresión se puede realizar sencillamente, como se muestra en 
el fragmento de código presentado en el Cuadro~\ref{lst:parsuma}, donde se busca paralelizar la evaluación
de los enteros que vienen como argumentos de \haskell{parsuma}, utilizando el operador \haskell{par},
y luego devolver la suma de los mismos. Podemos observar
gráficamente la expresión a evaluar en el Cuadro~\ref{fig:sumapar}, como al comenzar a evaluar la computación
se genera un \emph{spark} para evaluar la expresión de \haskell{x}, y continúa con la evaluación de la expresión
(\haskell{(+) x y}). Pero el programa mostrado en el Cuadro~\ref{lst:parsuma}, presenta un error sutil. 
La función \haskell{(+)} tal como se encuentra definida en el \emph{preludio} de Haskell, es estricta en su
primer argumento, por lo que evaluará primero a \haskell{x}. Es decir que el
valor del \emph{spark} generado por el combinador \haskell{par}
(para la evaluación de \haskell{x})
será necesitado inmediatamente al evaluar (\haskell{(+) x y}),
\textbf{eliminando} toda posibilidad de paralelismo. No puede haber paralelismo debido a que si todavía no fue evaluado,
será evaluado por el mismo hilo de la suma, y si está en proceso de evaluación, la computación esperará hasta que éste
haya terminado su evaluación, y luego continuará con la evaluación de \haskell{y}.

Una posible solución a este problema es intercambiar el orden de los
argumentos de la función \haskell{(+)} (ya que sabemos que es conmutativa) por \haskell{(y+x)},
pero esta idea
no es aplicable para cualquier función y puede ser que dada una función no sepamos el orden
de evaluación que ésta impone sobre sus argumentos. 

El combinador \haskell{pseq} permite establecer el orden de evaluación y de esta manera
\emph{secuencializar} la evaluación de \haskell{(x+y)} con el valor de \haskell{y} como
se muestra en el Código~\ref{lst:parpseqsuma}. De esta manera se logra tener paralelamente
dos hilos, uno para la evaluación de \haskell{x} y otro para la evaluación de \haskell{y}
que además realizará la suma de ambos cuando sus argumentos hayan sido evaluados,
independientemente del orden que impone la suma sobre sus argumentos.
Gráficamente en la figura presente en el Cuadro~\ref{fig:sumaparpseq}, observamos como al comenzar la evaluación de la expresión
\haskell{parsuma x y}, se genera un \emph{spark} para la evaluación de \haskell{x}, 
mientras se evalúa la expresión \haskell{pseq y (x + y)}, donde se evalúa 
primero a \haskell{y} y por último \haskell{(x + y)}.

\begin{table}
    \begin{tabular}{l r}
        \begin{subfigure}[b]{0.4\textwidth}
            \begin{lstlisting}
parsuma :: Int → Int → Int
parsuma x y =
   par x (pseq y (x+y))
            \end{lstlisting}
            \caption{Suma paralela correcta}
            \label{lst:parpseqsuma}
        \end{subfigure}
        & 
        \begin{subfigure}[b]{0.6\textwidth}
            \includegraphics[width=\textwidth]{sumaparpseq.pdf}
            %\includegraphics[scale=0.6]{sumaparpseq.pdf}
            \caption{Representación gráfica}
            \label{fig:sumaparpseq}
        \end{subfigure} \\
    \end{tabular}
    \caption{Suma utilizando \haskell{par} y \haskell{pseq}}
    \label{tab:sumaparpseq}
\end{table}


\section{Profundidad de la Evaluación}

Haskell, por defecto,  evalúa los valores a forma normal débil a la cabeza, es decir,
evalúa una computación hasta encontrar el primer constructor del tipo correspondiente. Por ejemplo,
la forma normal débil a la cabeza de un valor tipo lista es la lista vacía (\haskell{[]}) o una lista
con al menos un elemento (\haskell{_:_}). Es decir, la política de evaluación de Haskell es 
realizar el mínimo trabajo necesario, las expresiones son calculadas cuando son necesarias para continuar con la
ejecución del programa.

Los combinadores \haskell{par} y \haskell{pseq}, evalúan sus argumentos a forma normal débil
a la cabeza, por lo que el programador deberá asegurarse que los \emph{sparks} realmente estén
realizando el trabajo que se espera que realicen. Esta problemática es una de las principales
causas de que la programación paralela en lenguajes de evaluación perezosa sea difícil,
como es el caso de Haskell.
Exploremos un ejemplo para identificar la problemática, y presentar posibles soluciones.

La programación funcional incentiva a utilizar ciertos patrones como los \emph{funtores},
los cuáles nos permiten aplicar una función a los valores de un tipo de datos sin modificar la 
estructura, es decir,
nos permite modificar los valores que contiene la estructura, pero no la estructura en sí.
Particularmente
al trabajar sobre listas de elementos, es muy común utilizar la función \haskell{map}, la cual nos permite
aplicar una función a cada uno de los elementos de la lista.
\begin{lstlisting}
map :: (a → b) → [a] → [b]
map f [] = []
map f (x:xs) = (f x) : (map f xs)
\end{lstlisting}
Utilizando la función \haskell{map}, podemos manipular los elementos de una lista, sin pasar por estructuras
intermedias, ni cambiar de representación.
Por ejemplo, para sumarle uno a cada uno de los elementos de una lista de enteros, podemos escribir:
\begin{lstlisting}
sumauno xs = map (+1) xs
\end{lstlisting}

El mapeo de una función sobre la lista, es una función altamente paralelizable. Dado que se aplica
una misma función de forma independiente a cada uno de los elementos de la lista, podemos paralelizar la evaluación
de las aplicaciones. De forma muy intuitiva, una primer aproximación es utilizar el combinador \haskell{par},
como se muestra a continuación.
\begin{lstlisting}
parmap :: (a → b) → [a] → [b]
parmap f [] = []
parmap f (x:xs) = par (x') (x' : (parmap f xs))
    where
        x' = f x
\end{lstlisting}
Utilizando \haskell{parmap}, a medida que se va recorriendo la lista, se indica que la evaluación
de las expresiones \haskell{x'} (correspondiente a la evaluación de \haskell{f x}) se pueden realizar
de forma paralela. Por ejemplo,
si expandimos la expresión generada por \haskell{parmap (+1) [23,4,79014]} (ver Código~\ref{ej:parmap}),
vemos que se van generando \emph{sparks} que evalúan cada una de las aplicaciones de la función sobre los
elementos de la lista. Podemos observar el mismo comportamiento de manera gráfica
en la Figura~\ref{fig:parmap}.

\begin{program}
\begin{lstlisting}
parmap (+1) [23,4,79014] = par (23+1) ((23+1) : parmap (+1) [4,79014])
parmap (+1) [4,79014]    = par (4+1) ((4+1): parmap (+1) [79014])
parmap (+1) [79014]      = par (79014+1) ((79014+1) : [])
\end{lstlisting}
\caption{\haskell{parmap (+1) [23,4,79014]}}
\label{ej:parmap}
\end{program}

\begin{figure}
    \includegraphics[width=\textwidth]{parmap23479014.pdf}
    \caption{\haskell{parmap (+1) [23,4,79014]}}
    \label{fig:parmap}
\end{figure}

\begin{program}
\begin{lstlisting}
maybemap :: (a -> b) -> Maybe a -> Maybe b
maybemap f Nothing  = Nothing
maybemap f (Just x) = Just (f x)
\end{lstlisting}
\caption{Definición de \haskell{maybemap}}
\label{lst:maybemap:def}
\end{program}

Sin embargo, esta implementación de \haskell{parmap} \textbf{no} funciona como esperamos para todos los tipos
ya que en algunos casos los \emph{sparks} generados no realizarán suficiente
trabajo. Para el tipo \haskell{Int}, el comportamiento es correcto ya que su forma normal débil a la
cabeza coincide con su forma normal, y de esta manera los \emph{sparks} realizan el trabajo de evaluar
completamente los valores. Para hacer evidente este detalle, supongamos que tenemos ahora una lista
de posibles enteros,
\haskell{[Maybe Int]}, a los cuáles queremos sumarle uno. Definiendo una función
similar a \haskell{map} pero para elementos de tipo \haskell{Maybe} (ver Código~\ref{lst:maybemap:def}), 
podemos definir la siguiente expresión.
\begin{lstlisting}
parmap (maybemap (+1)) [Just 23, Just 4, Just 79014]
\end{lstlisting}
Podemos observar gráficamente en la Figura~\ref{fig:parmap:maybe}, que los
\emph{sparks} son creados
para la evaluación de expresiones de elementos \haskell{Maybe a}. El problema se origina dado 
que la forma normal débil a la cabeza de elementos de tipo \haskell{Maybe a} es, \haskell{Nothing}
o \haskell{Just x}, donde \haskell{x} \textbf{no} es evaluado. Es decir, si seguimos el gráfico,
los \emph{sparks} van a evaluar hasta encontrar el constructor \haskell{Just} y luego terminar su evaluación.
Si bien en el gráfico los valores se muestran, son simplemente para identificar las diferentes expresiones,
y no significa que los valores sean evaluados realmente.

\begin{figure}
    \includegraphics[width=\textwidth]{parmapmaybe23479014.pdf}
    \caption{\haskell{parmap (maybemap (+1)) [Just 23,Just 4,Just 79014]}}
    \label{fig:parmap:maybe}
\end{figure}

El combinador \haskell{pseq} permite no sólo establecer el orden de las computaciones,
sino también, establecer la profundidad a la cual los elementos serán evaluados, forzando de 
alguna manera la evaluación. Podríamos
introducir el combinador dentro de la función \haskell{parmap}, como se muestra a continuación.
\begin{lstlisting}
parmap f (x:xs) = par (pseq (f x) ()) ((f x) : (parmap f xs))
\end{lstlisting}
Pero estamos en exactamente la misma situación que se nos presentó anteriormente, debido a que \haskell{pseq}
simplemente evaluará a forma normal débil a la cabeza sus argumentos, del mismo modo que hace \haskell{par}.

Debemos aplicar el combinador \haskell{pseq} al elemento \emph{dentro} del tipo \haskell{Maybe a}.
Para esto definimos una función \haskell{posMaybe} que fuerza la evaluación de sus elementos:
\\
\begin{code}
posMaybe :: Maybe b → ()
posMaybe Nothing = ()
posMaybe (Just x) = pseq x ()
\end{code}

Notar que el tipo de retorno de la función \haskell{posMaybe} es \haskell{()}, lo que indica que
el valor de retorno \textbf{no} nos interesa, la expresión tiene el efecto de forzar la evaluación 
sin modificar ninguno de los valores que observa. Además, dicha función sólo nos permite
evaluar a forma normal débil a la cabeza el valor dentro de \haskell{Maybe b}, 
en el caso de ser un valor de la forma \haskell{Just x},
por lo que deberemos redefinir la función \haskell{parmap} especializando su tipo de retorno
a \haskell{Maybe b} como se muestra a continuación.

\begin{lstlisting}
maybeapp :: (a → Maybe b) → [a] → [Maybe b]
maybeapp f []     = []
maybeapp f (x:xs) = par (posMaybe (f x)) ((f x) : (maybeapp f xs))
\end{lstlisting}

%Al utilizar la función \haskell{posMaybe} para forzar la evaluación dentro de elementos tipo
%\haskell{Maybe b}, estamos restringiendo a que el valor de retorno de la función deba ser
%de tipo \haskell{Maybe b}.

\begin{figure}
    \includegraphics[width=\textwidth]{maybeapp.pdf}
    \caption{\haskell{maybeapp (maybemap (+1)) [Just 23, Just 4, Just 79014]}}
    \label{fig:maybeapp}
\end{figure}

Podemos observar de manera gráfica (ver Figura~\ref{fig:maybeapp}) el resultado de la evaluación de 
\haskell{maybeapp (maybemap (+1)) [Just 23, Just 4, Just 79014]}, donde al momento de evaluar los \emph{spark}
dedicados a la evaluación de expresiones de la forma \haskell{Just x}, también fuerzan la evaluación
de \haskell{x}, gracias a la aplicación de la función \haskell{posMaybe}.

Si bien esta solución realiza correctamente el trabajo que buscamos para 
elementos de tipo \haskell{Maybe Int}, no realiza el trabajo correcto en el caso
que el tipo dentro de \haskell{Maybe} tenga constructores, es decir, donde su forma normal
débil a la cabeza no coincida con su forma normal. Por ejemplo, \haskell{Maybe (Maybe Int)},
donde nos encontramos en un caso similar al comenzar a explorar la profundidad de la evaluación
que realizan \haskell{par} y \haskell{pseq}. Es posible continuar aplicando la función
\haskell{posMaybe}, y continuar especializando el tipo de \haskell{maybeapp}, pero no es una solución
definitiva. Siguiendo este camino deberíamos definir diferentes funciones especializadas en base a \haskell{parmap},
donde a los \emph{sparks} generados se les aplica una función para forzar la evaluación deseada.

La solución a este problema es definir la función \haskell{genparmap} donde tomamos como argumento
una función que indica cómo se tiene que realizar la evaluación de los
\emph{sparks} generados de forma similar a la función \haskell{posMaybe},
como se muestra a continuación.

\begin{lstlisting}
genparmap :: (b -> ()) -> (a -> b) -> [a] -> [b]
genparmap ev f []     = []
genparmap ev f (x:xs) =
    par (ev (f x))
        ((f x) : (genparmap ev f xs))
\end{lstlisting}

Particularmente, utilizando las funciones \haskell{genparmap} y
\haskell{posMaybe}, podemos redefinir la función \haskell{maybeapp} como se
muestra a continuación.

\begin{lstlisting}
maybeapp :: (a -> Maybe c) -> [a] -> [Maybe c]
maybeapp f xs = genparmap posMaybe f xs
\end{lstlisting}

Utilizando la función \haskell{genparmap} podemos evaluar la aplicación de una
función sobre los elementos de una lista en paralelo, pero la utilización de 
los combinadores \haskell{par} y \haskell{pseq} comienzan a obscurecer el código y 
se necesita entender completamente si los \emph{sparks} generan el trabajo esperado. Obteniendo
además un programa donde la definición del algoritmo (\haskell{map} en nuestro ejemplo)
se encuentra muy ligada a cómo el programador quiere que se evalué.

\section{Estrategias}

En la sección anterior analizamos el comportamiento de los combinadores \haskell{par} y \haskell{pseq},
y cómo éstos evalúan sus argumentos.
Mostramos
que el programador debe tener un conocimiento
completo sobre los valores que va a utilizar y forzar la completa evaluación de las computaciones dedicadas a la
evaluar dichos valores.
A su vez, el código
se plaga de los combinadores de paralelismo, obscureciendo el algoritmo. En esta sección se plantea
la utilización de \emph{Estrategias} como método para separar la definición del algoritmo
de la descripción de su evaluación.
A su vez, permite esclarecer y definir correctamente
el trabajo necesario para la evaluación completa de un tipo de datos concreto.

Definimos
como \emph{comportamiento dinámico} a toda función que describa como organizar las
computaciones, como ser, control del paralelismo (el uso de los combinadores \haskell{par} y \haskell{pseq}) o el
grado de evaluación de una computación. Es decir, el comportamiento dinámico es
toda función originada por el requerimiento
de incorporar paralelismo y que no sea parte del algoritmo. La función \haskell{posMaybe} definida anteriormente
es de comportamiento dinámico, ya que describe como evaluar los elementos de tipo \haskell{Maybe}, y sólo se 
requiere por su efecto en la evaluación.

A partir de \haskell{par} y \haskell{pseq} es posible construir un modelo más abstracto, donde se busca poder
separar lo más posible el comportamiento dinámico de la definición
de la función, simplificando la construcción de programas paralelos.
Para el control del paralelismo y el grado de evaluación de
expresiones dentro de Haskell, se introduce el concepto de
\emph{Estrategias}, funciones no estrictas polimórficas y de alto
orden~\parencite{Trinder:1998:ASP:969616.969618}. Estas funciones nos permiten
separar completamente la definición del algoritmo de su comportamiento dinámico.
%
%Se introduce el concepto de \emph{Estrategias}, funciones
%no estrictas polimórficas, y de alto orden, que permiten controlar el paralelismo y el grado de evaluación
%de las expresiones dentro de Haskell~\parencite{Trinder:1998:ASP:969616.969618}.
Las estrategias
sufrieron pequeñas modificaciones, debido principalmente a detalles de implementación,
hasta dar con la implementación actual definida por Simon Marlow~\parencite{strategies-2010}.
La estrategias se presentan entonces mediante el uso (interno) de la mónada Eval.

\begin{program}
    \begin{lstlisting}
data Eval a = Done a

instance Monad Eval where
    return x = Done x
    Done x >>= k = k x

runEval :: Eval a -> a
runEval (Done x) = x 

type Strategy a = a → Eval a

dot :: Strategy a -> Strategy a -> Strategy a 
s2 `dot` s1 = s2 . runEval . s1

using :: a → Strategy a → a
using x st = runEval (st x)

rpar :: Strategy a
rpar x = x `par` return x

rpseq :: Strategy a
rpseq x = x `pseq` return x

parList :: Strategy a → Strategy [a]
parList s []     = return []
parList s (x:xs) = do
    x'  <- rpar (s x)
    xs' <- (parList s xs)
    return (x':xs')
    \end{lstlisting}
    \caption{Definición de estrategias}
    \label{lst:strat}
\end{program}

Una estrategia de evaluación es una función que especifica el comportamiento dinámico
de un valor. No tiene contribución alguna al valor que está siendo computado
por el algoritmo, sino que sólo se utiliza por su \emph{efecto} en la evaluación del mismo.

Dentro de Haskell las estrategias son representadas como un tipo \haskell{Strategy} mostrado en el fragmento de Código~\ref{lst:strat},
en conjunto con operaciones básicas. Las estrategias son presentadas utilizando la mónada \haskell{Eval},
%aunque las estrategias pueden ser utilizadas de forma independiente,
el programador
puede elegir utilizar su interfaz monádica o no.
Utilizando esta definición,
podemos definir:
\begin{compactenum} 
    \item la composición de estrategias, \haskell{dot s2 s1}, como la estrategia que realiza todo el comportamiento
    dinámico definido por \haskell{s1} y luego todo el comportamiento dinámico definido por \haskell{s2},
    \item la aplicación de una estrategia sobre un valor, \haskell{using x s},
    realiza todo el comportamiento dinámico definido por \haskell{s} sobre \haskell{x}, y luego retorna \haskell{x},
    \item una estrategia \haskell{parList s xs}, que evalúa en paralelo el comportamiento
    dinámico definido por \haskell{s} para cada uno de los elementos de \haskell{xs},
    \item ciertas estrategias básicas como, \haskell{rpar x} que evalúa a
    \haskell{x} de forma paralela, o \haskell{rpseq x} que evalúa a \haskell{x}
    y luego continua normalmente.
\end{compactenum}
%
Utilizando la interfaz descripta de estrategias, se puede ocultar el uso de la mónada \haskell{Eval}, 
librando al programador de tener que modificar su programa para el manejo de efectos monádicos.
%la aplicación de una estrategia sobre un valor (\haskell{using})
%, como \haskell{rseq}, que evalúa 
%valores a su forma normal débil a la cabeza\footnote{Evaluación por defecto en Haskell.}, y es posible componer estrategias
%fácilmente utilizando el combinador \haskell{dot},
%Por último, se define la aplicación de una estrategia sobre un valor, como la evaluación de todo su comportamiento
%dinámico, y el retorno de su valor.
%%%%%%:%describir todas las operaciones!!

\begin{program}
    \begin{lstlisting}
class NFData a where
    rnf :: a -> ()

instance NFData a => NFData [a] where
    rnf [] = ()
    rnf (x:xs) = rnf x `pseq` (rnf xs)

instance (NFData a) => NFData (Maybe a) where
    rnf Nothing  = ()
    rnf (Just x) = rnf x
    \end{lstlisting}
    \caption{Evaluación de valores a formal normal}
    \label{lst:rnfdata}
\end{program}

Definiendo una nueva clase en Haskell \haskell{NFData} (ver Código~\ref{lst:rnfdata}) se puede representar el concepto
de forma normal de un tipo abstracto de datos, como una función que fuerza la evaluación completa
de una expresión de un tipo concreto, el cual no retorna elemento alguno.
Por ejemplo, para una lista de elementos,
su forma normal es valor resultante de la evaluación secuencial de sus elementos a forma normal.

Utilizando la clase \haskell{NFData} definida en el fragmento de Código~\ref{lst:rnfdata}, podemos redefinir 
la función \haskell{parmap} vista en la sección anterior, como se muestra a continuación.

\begin{lstlisting}
parmap :: (NFData b) => (a → b) → [a] → [b]
parmap f []     = []
parmap f (x:xs) = par (rnf (f x)) ((f x) : (parmap f xs))
\end{lstlisting}

La definición de la clase \haskell{NFData b} nos provee de la posibilidad de utilizar la función \haskell{rnf} sobre 
elementos de tipo \haskell{b} para forzar la completa evaluación de estos, de
manera que los sparks generados
por la función \haskell{parmap} realicen el trabajo esperado.

La clase \haskell{NFData}, nos permite además definir una nueva estrategia, \haskell{rdeepseq}, que 
evalúa una expresión a su forma normal.

\begin{lstlisting}
rdeepseq :: NFData a => Strategy a
rdeepseq x = pseq (rnf x) (return x)
\end{lstlisting}

Al definir la evaluación a forma normal como una estrategia, nos permite combinarla con las definidas anteriormente,
como ser \haskell{parList}~(Código~\ref{lst:strat}), y definir la evaluación paralela de una lista como se muestra a
continuación.

\begin{code}
parmap :: NFData b => (a → b) → [a] → [b]
parmap f xs = map f xs `using` parList rdeepseq
\end{code}

La función \haskell{using} nos permite separar totalmente el algoritmo (\haskell{map}) de
su comportamiento dinámico (\haskell{parList rdeepseq}), permitiéndole al programador que pueda
desarrollar por separado el algoritmo de la forma en que será evaluado.
De esta manera, simplemente se tiene que definir una estrategia que identifique la reducción
de una expresión a forma normal de un tipo determinado, y ya es posible asegurar
que los \emph{sparks} que son creados realizan el trabajo esperado.
%paralelizar correctamente
%las aplicaciones de una lista.

Utilizando el modelo de estrategias se puede especificar el comportamiento dinámico
por separado del algoritmo muy fácilmente, permitiendo paralelizar el algoritmo
mediante una estrategia acorde a la necesidad.

\section{Mónadas para el Paralelismo}

Además de la mónada Eval, utilizada internamente por las estrategias, se
encuentra la mónada Par~\parencite{citeulike:12605079}. Internamente utiliza
primitivas de concurrencia en conjunto con un mecanismo de comunicación entre procesos basado en
\emph{IVars}~\parencite{Arvind:1989:IDS:69558.69562}, que permite definir variables donde van a ser
alojados los resultados de las computaciones, como es el caso de \haskell{xf} en el ejemplo mostrado
debajo. En el trabajo presentado por Simon Marlow~\parencite{citeulike:12605079} se muestra que
es posible conseguir un modelo de paralelismo totalmente determinístico basado
en primitivas de concurrencia.

Por ejemplo, escribimos la función Fibonacci en ambas mónadas como se muestra a continuación.
\begin{table}[ht]
    \begin{tabular}{l r}
        \begin{subfigure}[b]{0.5\textwidth}
            \begin{lstlisting}
parfib :: Int → Par Int
parfib n | n < 2 = return 1
parfib n = do
    xf <- spawn_ $ parfib (n-1)
    y  <- parfib (n-2)
    x  <- get xf
    return (x+y)
            \end{lstlisting}
            \caption{Fibonacci utilizando la mónada Par}
            \label{lst:fibonacci:parmonad}
        \end{subfigure}
        & 
        \begin{subfigure}[b]{0.6\textwidth}
            \begin{lstlisting}
strfib :: Int → Int
strfib n | n < 2 = 1
strfib n = runEval $ do
    x <- rpar (strfib (n-1))
    y <- rseq (strfib (n-2))
    return (x+y)
            \end{lstlisting}
            \caption{Fibonnaci utilizando la mónada Eval}
            \label{lst:fib:eval}
        \end{subfigure} \\
    \end{tabular}
    \caption{Comparación entre mónadas Par y Eval}
\end{table}

La función Fibonacci implementada como se muestra en el fragmento de código del Cuadro~\ref{lst:fibonacci:parmonad},
toma un entero, \haskell{n}, y devuelve \haskell{fib n} dentro de la mónada Par. Para los casos bases simplemente retorna 1,
mientras que para $n >= 2$, genera un nuevo hilo al cual le asigna el trabajo de evaluar la expresión 
\haskell{parfib (n-1)}, se realiza el calculo de \haskell{parfib (n-2)} y luego se suman. Internamente,
como mencionamos en el párrafo anterior, la mónada Par utiliza \emph{IVars}, las cuales son variables
estrictas, es decir, sólo comunican valores en forma normal, obligando a que el nuevo hilo evalúe completamente la
expresión. Mientras que en la implementación utilizando la mónada Eval (ver
Cuadro~\ref{lst:fib:eval}), simplemente utiliza
de forma monádica las estrategias \haskell{rpar} y \haskell{rseq}.

En el código se observa que el programador está obligado a utilizar un estilo monádico de programación.
Utilizando estrategias es posible reducir el impacto, restringiendo 
a que sólo se trabaje con mónadas al momento de definir nuevas estrategias. Por ejemplo, se define una
estrategia de evaluación para la suma y luego se la aplica utilizando \haskell{using}, como se muestra
en el fragmento de código a continuación.

\begin{code}
parfib n = x + y `using` strat
where
    x = strfib (n-1)
    y = strfib (n-2)
    strat v  = do rpar x; rseq y; return v
\end{code}
