\chapter{Observar Paralelismo con \nombre{}}

En este capítulo se muestra cómo utilizar a la herramienta
\nombre{} para el análisis de programas paralelos.
Gracias a los gráficos generados por la herramienta, es posible analizar
programas paralelos que utilizan los combinadores \haskell{par} y
\haskell{pseq}.

Utilizando particionamiento semi-explícito el programador
debe dar indicaciones sobre qué es lo que quiere paralelizar 
(\haskell{par}) y cómo ordenar las computaciones (\haskell{pseq}). A partir
de estos combinadores básicos, se construye un modelo de más
alto nivel, que nos permite separar el comportamiento dinámico
del algoritmo, las estrategias. Pero 
el programador \textbf{no} tiene forma de observar directamente las construcciones
dinámicas de sus programas.
Esto lleva a que la construcción de programas paralelos
en Haskell sea complicada, se necesite un gran entendimiento a bajo
nivel
y se base fuertemente en la experiencia previa.

Se presenta una nueva herramienta para complementar la visión que tiene
el programador sobre su programa y exponer directamente el comportamiento dinámico del
mismo. El objetivo es generar gráficos de forma automática
que ilustren el comportamiento de las computaciones.
Todos los gráficos del capítulo anterior, los cuales visualizan 
el comportamiento dinámico de una expresión, fueron generados con \nombre{}. 

Repasaremos como utilizar a \nombre{}, dado que el proceso no se encuentra totalmente automatizado,
por lo que es necesario que el usuario modifique levemente sus programas.

\section{Operadores Básicos}

Para poder graficar el comportamiento dinámico de un programa paralelo es
necesario realizar algunas modificaciones.
%
Los operadores básicos de paralelismo, se reemplazan por los operadores
mostrados en el fragmento de Código~\ref{lst:parpseq}. Estos nuevos operadores permiten recopilar información
del comportamiento dinámico de la evaluación de las expresiones.

\begin{program}
    \begin{lstlisting}
par    :: TPar a -> TPar b -> TPar b
pseq   :: TPar a -> TPar b -> TPar b
mkVar  :: a -> TPar a

(<$>) :: (a -> b) -> TPar a -> TPar b
(<*>) :: TPar (a -> b) -> TPar a -> TPar b
    \end{lstlisting}
    \caption{Combinadores básicos de paralelismo y operadores básicos de \haskell{TPar}}
    \label{lst:parpseq}
\end{program}

Se introduce, además, el operador \haskell{mkVar}, el cual nos permite introducir valores dentro del
tipo \haskell{TPar}.
El tipo de datos \haskell{TPar x} modela a \haskell{x} con cierto comportamiento dinámico adicional.
Capturamos el comportamiento de los operadores básicos de paralelismo de Haskell, e introducimos
los operadores \haskell{(<$>)} y \haskell{(<*>)}, para poder operar sobre los elementos que son representados.
De esta manera podemos reemplazar los operadores de paralelismo, por los presentados en el fragmento 
de Código~\ref{lst:parpseq}, de forma casi directa. 

Utilizando los combinadores presentados en el Código~\ref{lst:parpseq}, reescribimos el
ejemplo de una implementación paralela de la suma (\haskell{parsuma}, vista en el capítulo anterior)
como se muestra en el Cuadro~\ref{tab:sumapar:tpar}, donde se comparan ambas versiones. A izquierda
encontramos la versión utilizando los combinadores de Haskell, y a derecha utilizando los combinadores
de \nombre{}.
\begin{table}
    \begin{tabular}{l r}
        \begin{subfigure}[b]{0.4\textwidth}
\begin{lstlisting}
parsuma :: Int -> Int -> Int
parsuma x y =
    par x
    (pseq y
    (x + y))
\end{lstlisting}
            \caption{Suma paralela.}
            \label{lst:parsuma:norm}
        \end{subfigure}
        & 
        \begin{subfigure}[b]{0.6\textwidth}
\begin{lstlisting}
parsuma :: Int -> Int -> TPar Int
parsuma x y = par x'
              (pseq y' (x' + y'))
   where
      x' = mkVar x
      y' = mkVar y
\end{lstlisting}
            \caption{Suma paralela utilizando a \nombre{}.}
            \label{lst:sumapar:tpar}
        \end{subfigure} \\
    \end{tabular}
    \caption{Comparación entre el código original y el código modificado.}
    \label{tab:sumapar:tpar}
\end{table}

\begin{figure}
    \includegraphics[width=0.8\textwidth]{sumaparpseq.pdf}
    \caption{Gráfico del comportamiento dinámico de \haskell{parsuma x y}}
    \label{img:tparpseqsuma}
\end{figure}

Utilizando las construcciones generadas por los operadores de \nombre{}, se puede
generar un gráfico de la computación, representando el comportamiento dinámico.
El gráfico se genera a partir de la ejecución de la computación. Por ejemplo, la
Figura~\ref{img:tparpseqsuma} es el resultado
de la ejecución de \haskell{parsuma x y}, donde tanto \haskell{x} como \haskell{y} toman
valores enteros.

Las computaciones son representadas en forma de un árbol,
los cuales pueden comenzar del lado izquierdo y crecer hacia la derecha,
o comenzar desde arriba y crecer hacia abajo.
El constructor \haskell{par a b} es representado como una \emph{elipse}, 
de donde salen 2 lineas, la linea segmentada indica la creación del \emph{spark}
destinado a evaluar la expresión \haskell{a}, y la linea continua indica la 
la evaluación de la
expresión \haskell{b}. El constructor \haskell{pseq a b} es representado como
un \emph{octágono}, donde se indica con una linea de puntos la evaluación de la
expresión \haskell{a}, y con una linea más gruesa la evaluación de la expresión \haskell{b}.
Los valores son representados como cajas.
Como podemos ver en al Figura~\ref{img:tparpseqsuma}, el gráfico comienza con
una elipse, mostrando la presencia del operador \haskell{par}, el cual crea un
\emph{spark} destinado a la evaluación de la expresión
\rhaskell{x} y continua con la evaluación de un octágono, es decir, un \haskell{pseq}, que
evalúa primero la expresión \rhaskell{y}
y luego evalúa el resultado \rhaskell{(+) x y}.
Podemos observar entonces, que el
grafo resultante se encuentra ligado al algoritmo implementado, y de esta manera
se puede observar directamente el comportamiento dinámico del programa.

Utilizando esta representación gráfica, el valor de toda la
expresión se encuentra descripto por el último valor resultante de
seguir siempre las líneas continuas.

\section{Profundidad de la evaluación}

En el Capítulo 2, mostramos que paralelizar computaciones de forma naif produce
que los \emph{sparks} generados puedan no llegar a realizar el trabajo que uno espera,
terminando con un programa
al cual se le agregan nuevas directivas y por ende es más complicado, sin
obtener ganancias en velocidad.
Pero luego de un cierto análisis de las computaciones que se paralelizan, alcanzamos un punto donde el
trabajo se realiza de manera efectiva, optimizando la ejecución del programa.
Ello se logra al
encontrar la profundidad de evaluación correcta de las expresiones, la cual no resulta ser extremadamente difícil
en un pequeño ejemplo, pero a medida que se van haciendo más complejas las construcciones, es cada vez
más difícil identificar correctamente la profundidad buscada.

Utilizamos a \nombre{} para observar directamente el comportamiento dinámico de
los programas, y esclarecer la profundidad de la evaluación. Para esto,
mostramos cómo el programa debe ser modificado, utilizando los programas 
presentados en el capítulo~2.

Modificaremos la función \haskell{parmap}, para poder generar elementos dentro de \haskell{TPar},
simplemente reemplazando el operador \haskell{par} de la librería
\haskell{Control.Parallel} por el operador \haskell{par} de \nombre{},
las aplicación por los operadores \haskell{(<$>)} y \haskell{(<*>)} según corresponda, y agregando también
\haskell{mkVar} cuando sea necesario.
Podemos observar la comparación entre ambos códigos en el Cuadro~\ref{tab:parmap:tpar}.

\begin{table}
    \begin{tabular}{l r}
        \begin{subfigure}[b]{0.4\textwidth}
\begin{lstlisting}
parmap :: (a -> b) -> [a] -> [b]
parmap f [] = []
parmap f (x:xs) =
    par x'
    (x' : (parmap f xs))
    where
        x' = f x
\end{lstlisting}
            \caption{Clásico \haskell{parmap}.}
            \label{lst:parmap:norm}
        \end{subfigure}
        & 
        \begin{subfigure}[b]{0.6\textwidth}
\begin{lstlisting}
parmap :: (Show a) => (a -> b) -> [a] -> TPar [b]
parmap f [] = mkVar []
parmap f (x:xs) =
    par x'
    ((:) <$> x' <*> (parmap f xs))
    where
        x' = mkVars $ f x
\end{lstlisting}
            \caption{\haskell{parmap} utilizando a \nombre{}.}
            \label{lst:parmap:tpar}
        \end{subfigure} \\
    \end{tabular}
    \caption{Comparación entre el código original y la incorporación de la herramienta.}
    \label{tab:parmap:tpar}
\end{table}

A diferencia de la versión normal, ahora es posible generar el grafo que
representa la evaluación de la computación, mediante la función \haskell{graficar}.

\begin{lstlisting}
graficar :: Bool -> TPar a -> String -> IO FilePath
withCanv :: Bool -> TPar a -> IO ()
\end{lstlisting}
%
La función \haskell{graficar}, toma como argumento un elemento de tipo \haskell{Bool},
que indica si se quiere observar o no los identificadores únicos de los nodos. En el 
caso que reciba como argumento el valor \haskell{True}, los identificadores se mostraran
en conjunto con las etiquetas de los nodos. Si un nodo ya posee una etiqueta, se separan
mediante el símbolo @, y en el caso que no posea etiqueta simplemente se mostrará el identificador.
Estos identificadores sirven como referencia sobre los nodos, y pueden ser útiles
cuando se utiliza el intérprete \emph{GHCi}, el cual permite interactuar con el código.
La función \haskell{withCanv},
nos permite interactuar dentro del intérprete \textit{GHCi}, genera el gráfico y lo muestra sin tener
que cerrar el intérprete.

\begin{figure}
    \includegraphics[width=\textwidth]{parmaybeVerbose.pdf}
    \caption{\haskell{graficar True (parmap (maybemap (+1)) [Just 23, Just 4, Just 79014])}}
    \label{fig:parmap:maybe:cap3}
\end{figure}

La función \haskell{graficar} nos permite generar un gráfico, como el presentado
en la Figura~\ref{fig:parmap:maybe:cap3}.
Éste gráfico muestra explícitamente las operaciones que fueron realizadas, así como también
muestra una identificación, la cual se observa dentro de los nodos a la derecha del símbolo @ 
en el caso que tengan una etiqueta con mas información (como ser, 
\lstinline[keywordstyle=\color{red},basicstyle=\footnotesize\rmfamily\color{red}]{Just 24}{\footnotesize\rmfamily @2})
o simplemente el identificador (como ser, el primer nodo con identificador 1).
Estos identificadores fueron asignados a los nodos durante la ejecución de la herramienta.
En dicha figura se observa directamente cuales son las expresiones que los
diferentes \emph{sparks} evalúan.

Repasemos cómo es la evaluación de la expresión
\\
\begin{code}
parmap (maybemap (+1)) [Just 23, Just 4, Just 79014]},
\end{code}\\
%,
donde \haskell{parmap} aplica una función a una lista de elementos, paralelizando la evaluación de la
aplicación de la función sobre cada uno de los elementos.
Al comenzar a recorrer la lista, se crea un \emph{spark} dedicado a la evaluación de \haskell{maybemap (+1) (Just 23)},
y luego continua con el resto de los elementos, generando los \emph{sparks} correspondientes. En la Figura~\ref{fig:parmap:maybe:cap3},
podemos observar como se genera un \emph{spark} dedicado a la evaluación de los diferentes elementos, pero
los \emph{sparks} no realizan todo el trabajo. Dada la evaluación a forma normal débil a la cabeza impuesta por el
operador \haskell{par} sobre los \emph{sparks} que son creados, la expresión \haskell{maybemap (+1) (Just 23)} será
evaluada de la siguiente forma, se aplicará la función \haskell{maybemap}, obteniendo la expresión
\haskell{Just ((+1) 23)}, y terminará la evaluación dado que ya se habrá alcanzado la forma normal débil a la cabeza
para elementos de tipo \haskell{Maybe Int}. Gráficamente, este comportamiento lo podemos observar gracias a las etiquetas
de los nodos. Al comenzar la evaluación de la expresión que representa el nodo
1, se genera un \emph{spark} para la evaluación
de la expresión presentada en el nodo con identificación 2, donde éste evalúa la expresión hasta encontrar el constructor
\lstinline[keywordstyle=\color{red},basicstyle=\footnotesize\rmfamily\color{red}]{Just} dejando sin evaluar el resto de
la expresión, en este caso \haskell{(+1) 23}.

El mismo fenómeno sucede con los nodos 4 y 6. De este modo, la operación que buscamos que se realice en paralelo,
\haskell{(+1)} sobre cada uno de los elementos de la lista, no ha sido realizada como esperábamos, es decir,
no se paralelizó la aplicación completa a los elementos de la lista, sino que al momento de observar la
lista resultante las sumas faltantes (\haskell{(+1) 23}, \haskell{(+1) 4},
\haskell{(+1) 79014}) se hacen de forma secuencial.

\begin{program}
    \begin{lstlisting}
posMaybe :: b -> TPar ()
posMaybe Nothing  = mkVar ()
posMaybe (Just x) = pseq x (mkVar ())

maybeapp :: (Maybe a -> Maybe b) -> [Maybe a] -> TPar [Maybe b]
maybeapp f [] = mkVars []
maybeapp f (x:xs) =
    par (posMaybe =<< x')
        ((:) <$> x' <*> (maybeapp f xs)) 
    where
        x' = mkVars (f x)
    \end{lstlisting}
    \caption{Versión paralela correcta de maybeapp utilizando a \nombre{}}
    \label{lst:herr:forceqsort}
\end{program}

\begin{figure}
    \includegraphics[width=\textwidth]{maybeapp.pdf}
    \caption{Gráfico resultado de: \haskell{graficar False (maybeapp [Just 23, Just 4, Just 79014])}}
    \label{fig:maybeapp:cap3}
\end{figure}

Para garantizar que el trabajo se realice dentro de los \emph{sparks} que son
generados, utilizamos la función \haskell{posMaybe} antes presentada modificada para usar el EDSL.
La función ahora
retorna un \haskell{TPar ()} e inserta los valores a
representar utilizando el operador \haskell{mkVar} 
(ver Código~\ref{lst:herr:forceqsort}).
Debido a las modificaciones realizadas, el tipo resultante de la función
\haskell{posMaybe} es \haskell{b -> TPar()}, pero los valores a manipular son de tipo
\haskell{TPar b}, con lo que podemos utilizar la aplicación monádica
\haskell{(=<<)} que permite acumular el comportamiento dinámico generado por la
función \haskell{posMaybe}.
De esta manera generamos el grafo mostrado en la Figura~\ref{fig:maybeapp:cap3}.
Podemos observar la aparición de nuevos rombos, representando
la mayor utilización de \haskell{pseq}. Particularmente, dichos rombos se encuentran en lugar de los
valores en el gráfico anterior (Figura~\ref{fig:parmap:maybe:cap3}). Ahora podemos observar
como la función \haskell{posMaybe}, ha agregado el comportamiento dinámico necesario, forzando la evaluación
a forma normal débil a la cabeza de los diferentes valores dentro del tipo
\haskell{Maybe}.
%
 
Mediante los gráficos generados pudimos observar directamente la necesidad de una función \haskell{posMaybe} para
forzar la evaluación completa, y al hacerlo también se pudo analizar el trabajo
que realizan los \emph{sparks}.

\section{Estrategias dentro de \nombre{}}

Las estrategias buscan separar el comportamiento dinámico lo
más posible de la implementación del algoritmo. Esta separación se logra
definiendo el comportamiento dinámico de los diferentes tipos de datos a utilizar, y su
nivel de evaluación, permitiendo mezclar el comportamiento dinámico y los niveles de evaluación
para alcanzar el paralelismo buscado.

%En el capítulo mencionamos que Simon Marlow realizo un cambio sutil sobre las estrategias
%originales. Debido a que dentro de la herramienta queremos representar las estructuras dinámicas
%generadas por el uso de los combinadores de paralelismo, representaremos las estrategias como
%están definidas actualmente en el paquete \emph{Control.Parallel.Strategies}.

Utilizaremos a \nombre{} para observar el comportamiento de las estrategias
tal cual se encuentran definidas en la versión actual de GHC dentro del paquete
\emph{Control.Parallel.Strategies}~\parencite{ghc_strat}.
%Primero redefiniremos el tipo de las estrategias, de manera tal que nos permita capturar el
%comportamiento dinámico.

Las estrategias son funciones que describen el comportamiento dinámico
sobre un elemento. Éste comportamiento dinámico adicional define
cuales expresiones serán evaluadas en paralelo, y cuan profunda es la
evaluación de los elementos.
Al momento de realizar un análisis sobre las estrategias
uno espera poder observar la estructura generada al aplicar una estrategia particular
sobre un elemento.
Utilizando el EDSL de \nombre{}, las estrategias son simplemente funciones
que dado un elemento pueden construir la estructura que describe su comportamiento
dinámico, por lo que son redefinidas como se muestra a continuación.

\begin{lstlisting}
type Strategy a = a -> TPar a
\end{lstlisting}

Al redefinir las estrategias, es necesario realizar ciertas modificaciones sobre las
funciones básicas de las estrategias, aunque estos cambios son sencillos
e intuitivos. A modo de ejemplo, mostramos como modificar la estrategia \haskell{parList}.
%
\begin{code}
parList :: Strategy a -> Strategy [a]
parList st []     = mkVar []
parList st (x:xs) =
    par (st =<< x')
        ((:) <$> x' <*> (parList st xs))
    where
        x' = mkVar x
\end{code}\\
Donde se insertan los elementos utilizando el operador \haskell{mkVar}, se
reemplazan las aplicaciones de funciones por los operadores \haskell{(<$>)},
\haskell{(<*>)} y la aplicación monádica \haskell{(=<<)} que nos permite acumular
el comportamiento dinámico.

Una vez definidas las estrategias en base al tipo \haskell{TPar}, podemos modificar la implementación
de \haskell{parmap}, de la misma forma que fue definida en el Capítulo 2.
\begin{lstlisting}
parmap :: (NFData b) => (a -> b) -> [a] -> TPar [b]
parmap f xs = map f xs `using` parList rdeepseq 
\end{lstlisting}

Definiendo las estrategias utilizando el tipo \haskell{TPar}, obtenemos la habilidad de graficar los
resultados, sin tener que modificar el código ya programado en la mayoría de los
casos. Es decir, en código donde el comportamiento dinámico estaba dado por una
estrategia, sólo es necesario modificar levemente la estrategia para poder
utilizar nuestra herramienta.

\section{Caso de estudio: Map Reduce}
MapReduce es un patrón de diseño para el cómputo de programas sobre grandes
cantidades de información.
El usuario específica una función denominada \textit{map},
que procesa un par clave-valor para generar un conjunto de pares clave-valor
intermedios, los cuales son ordenados y recolectados en base a la clave
intermedia para ser utilizados por una función, denominada \textit{reduce}, que sintetiza todos los
valores de una misma clave intermedia~\parencite{Dean:2008:MSD:1327452.1327492}.
El modelo, además, tiene la característica que es muy fácil de paralelizar,
principalmente debido a la naturaleza de la aplicación de la función \textit{map}. Debido a que la
aplicación de la función es aplicada a cada elemento, es decir, la aplicación es independiente del
resto de los elementos, permitiendo distribuir todas las expresiones correspondientes
a las aplicaciones sobre los procesadores disponibles. Luego de terminar las computaciones,
los valores son recolectados para ser sintetizados por la función \textit{reduce}.

Siguiendo el ejemplo de libro \emph{Real World
Haskell}~\parencite{real_world_haskell}
podemos obtener una función basada en el funcionamiento general de \textit{Map Reduce},
que nos permite operar de manera \textbf{similar} a la antes planteada. Son
necesarias dos funciones: primero la función \textit{map} de tipo \haskell{a -> b}, que será aplicada a una lista de valores
de tipo \haskell{a}; en segundo lugar, necesitamos una función \textit{reduce} que
sintetice los valores generados por la función \textit{map} de tipo \haskell{[b] -> c}.
A continuación se muestra la versión secuencial del algoritmo.
\begin{lstlisting}
simpleMapReduce ::
           (a -> b)     
        -> ([b] -> c)   
        -> [a]
        -> c
simpleMapReduce mapper reducer xs = reducer $ map mapper xs
\end{lstlisting}

Dadas las funciones \haskell{mapper} y \haskell{reducer}, y una lista \haskell{xs},
simplemente aplicamos la función \haskell{mapper} a cada uno de los elementos de 
\haskell{xs}, y luego sintetizamos todos los resultados utilizando \haskell{reducer}.

En el algoritmo presentado, es posible explotar el paralelismo de la evaluación de la lista
utilizada, como ser, utilizando la estrategia \haskell{parList}, u otra
estrategia de evaluación sobre listas de elementos, por lo que se toma como
argumento la estrategia que desea aplicar el usuario.
%dando la necesidad de
%obtener como argumento una estrategia de evaluación de una lista de elementos de tipo \haskell{b}.
Además,
es posible que el elemento generado por la función \haskell{reducer}, pueda ser evaluado 
de forma paralela, dando la necesidad de agregar un último argumento para la estrategia de
evaluación de elementos de tipo \haskell{c}. De esta manera, podemos aplicar
estrategias para dar una definición del algoritmo
%\textit{Map Reduce}
que explote
el paralelismo del programa.

\begin{code}
mapReduce :: 
       Strategy [b]    -- ^ Estrategia evaluación de lista de b
    -> (a -> b)        -- ^ Map
    -> Strategy c      -- ^ Estrategia evaluación de c
    -> ([b] -> c)      -- ^ Reduce
    -> [a]
    -> TPar c
mapReduce strb mp strc red xs = pseq mapRes redRes
    where
        mapRes = map mp xs `using` strb
        redRes = (red <$> mapRes) `rusing` strc
\end{code}

A medida que se acumula comportamiento dinámico, el programador ya no tiene acceso a elementos
\haskell{x} de tipo \haskell{a}, sino que manipula elementos con cierto comportamiento dinámico
adicional, \haskell{x'} de tipo \haskell{TPar a}. Debido a esto, se introduce una nueva aplicación de estrategias,
que nos permite operar de la misma forma, acumulando nuevo comportamiento dinámico.
A continuación se muestra la interfaz de la función.
\begin{lstlisting}
rusing :: TPar a -> Strategy a -> TPar a
\end{lstlisting}

Utilizando esta estructura propuesta por \haskell{mapReduce}, podemos dar un algoritmo,
por ejemplo, para contar la aparición de las palabras en un texto dado, 
como se muestra en el fragmento de Código~\ref{lst:mapred}. Donde encontramos las funciones:
\begin{compactitem}
    \item \haskell{oneWord}, que dada una palabra le asigna el valor 1,
    \item \haskell{summ}, que suma todos los valores asignados a una palabra
    específica,
    \item \haskell{normalize}, recorre la lista utilizando \haskell{summ} para acumular
    los resultados,
    \item \haskell{rightTup}, es una estrategia sobre pares de tipo \haskell{(String, Int)},
    donde solamente evalúa
    a forma normal la componente derecha del par, y asigna las etiquetas
    correspondientes, 
    para luego en el gráfico saber a que palabra hace referencia un nodo.
\end{compactitem}

\begin{program}
\begin{lstlisting}
oneWord :: String -> (String, Int)
oneWord s = (s,1)

summ :: [(String,Int)] -> String -> Int -> [(String, Int)]
summ [] s i = [(s,i)]
summ ((s,i):xs) ns ni   | (s == ns) = (s,i+ni) : xs
                        | s < ns = (s,i) : (summ xs ns ni)
                        | otherwise = (ns, ni) : ((s,i):xs)

normalize :: [(String,Int)] -> [(String,Int)]
normalize xs = foldl (\ a (w,i) -> summ a w i) [] xs

rightTup :: Strategy (String,Int)
rightTup (a,b) = mkVar (,) <*> (vlbl a (r0 a)) <*> (vlbl (show b) (rwhnf b))

parContPal :: [String] -> TPar [(String, Int)]
parContPal xs = mapReduce (parList rightTup) oneWord
    (parList rightTup) normalize xs

parContPalC :: [String] -> TPar [(String, Int)]
parContPalC xs = mapReduce (parListChunk 4 rightTup) oneWord
    (parListChunk 4 rightTup) normalize xs

ejemplo :: [String]
ejemplo = "contar saltar contar pegar"

ejemplo2 :: String
ejemplo2 = "Al fin llego el día en que Martín va a dejar de molestar a los profesores."

resultado :: [(String, Int)]
resultado = parContPal (splitOn " " ejemplo)

resultado2 :: [(String, Int)]
resultado2 = parContPal (splitOn " " ejemplo2)

\end{lstlisting}
\caption{Contar palabras utilizando \haskell{mapReduce}.}
\label{lst:mapred}
\end{program}

El resultado de la computación \haskell{resultado}, es una lista de pares
que indican cuantas apariciones en el texto \haskell{ejemplo} tiene la 
primer componente.

\begin{figure}
    \includegraphics[width=\textwidth]{molestar.pdf}
    \caption{Gráfico representando la estructura dinámica de \haskell{resultado}}
    \label{fig:mr:ej}
\end{figure}

Podemos observar el gráfico resultante de la computación de contar las
palabras dentro del pequeño ejemplo, en la Figura~\ref{fig:mr:ej}.
La aplicación de la función \haskell{oneWord} es aplicada
a cada uno de los elementos de la lista
\\ \haskell{["contar","pegar","contar","saltar"]}, y luego, la lista
resultante es sintetizada por la función \haskell{normalize}, la cual suma la cantidad de apariciones de 
cada palabra.

Gracias al gráfico mostrado en la Figura~\ref{fig:mr:ej} podemos observar, que
se genera un \emph{spark} para la evaluación de \haskell{oneWord} sobre cada una de las
palabras, sumando que conocemos que la aplicación de la función no requiere mucho trabajo,
resulta en un paralelismo muy granular. 
Es decir, la evaluación de la expresión \haskell{resultado}, genera \textit{sparks}
con poco trabajo a realizar. Esto produce que el costo de evaluar la expresión en paralelo sea mayor que
la evaluación de la expresión sí. Una forma sencilla de evitar este problema, es particionar la lista
en pequeñas lista, llamadas \emph{chunks}, y evaluar cada una de estas listas por separado, es decir,
acumular trabajo
suficiente y luego distribuirlo por los diferentes núcleos. Utilizaremos a \haskell{resultado2} como
ejemplo, debido que \haskell{parContPalC} divide la lista de palabras en listas de 4 palabras. El
resultado se puede apreciar en la Figura~\ref{fig:mr:ej2}, donde podemos observar
que se paralelizan las aplicaciones de a 4 elementos.

\begin{figure}
    \includegraphics[width=\textwidth]{chunks.pdf}
    \caption{Gráfico representando la estructura dinámica de \haskell{resultado2}}
    \label{fig:mr:ej2}
\end{figure}

%%%%%%
\subsection{Resumen}

Durante el capítulo desarrollamos como utilizar la herramienta para observar
la estructura dinámica de las computaciones basadas en los operadores básicos
\haskell{par} y \haskell{pseq}.
%Cómo, en conjunto con el código,
%los gráficos nos permitieron hacer un análisis más exhaustivo sobre nuestros
%programas.
Se logró identificar si las expresiones realizaban el trabajo
esperado o si la evaluación perezosa impedía que se terminara de evaluar toda la
expresión, y se detectó la generación de \emph{sparks} demasiado granulares.
Si bien los programas sufren leves modificaciones, nos entregan
un mapa de la información estática sobre el comportamiento dinámico de la
expresión, lo que nos permite entender mejor los programas a paralelizar.
