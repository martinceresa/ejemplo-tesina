# Ejemplo de Tesina #

Buenas! Hago publico el código fuente de mi tesina, así capaz que le sirva a alguien para hacer la suya!

Traté de ser lo más limpio posible, y dejar todo bastante separado.
Ahora voy a hacer un repaso corto de donde se encuentra cada cosa.

En el directorio raíz del proyecto van a encontrar:

* Tesis.tex : este es el archivo central de la tesina, el cual importa las configuraciones (settings.mem.tex), la caratula (title.tex), el resumen (abstract.tex), los agradecimientos (thanks.tex),
y luego importara los diferentes capítulos.

* settings.mem.tex : aquí se encuentran todos los paquetes y configuraciones necesarias, para dejar lo más limpio posible a Tesis.tex

* title.tex : la caratula...

* abstract.tex y thanks.tex : resumen y agradecimientos.

## Presentación ##

La presentación la hice con beamer, y se encuentra en el archivo slidex.tex.

## Problemas ##

La clase memoir tiene conflictos con el paquete subcaption. En mi computadora funcionó bien hasta el final y no lo quería cambiar, si alguien usa esto, trate de evitar el paquete subcaption.

## Herramienta ##

Para dar la presentación use xpdf, que permite abrir sesiones remotas y navegar desde la terminal. Así que me hice un pequeño script bash que me mostraba la slide siguiente y las notas que había generado con beamer.
El script se llama xpdfwithnotes, por si a alguien le interesa.
